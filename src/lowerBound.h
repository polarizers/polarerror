 /* LowerBound.h
 *
 * Copyright (C) 2018 Boaz Shuval <bshuval@campus.technion.ac.il> and Ido Tal <idotal@ieee.org> 
 *
 * This file is part of polarError. If you wish to use it, please cite: 
 * B. Shuval, I. Tal ``A Lower Bound on The Probability of Error of Polar Codes over BMS Channels,''
 * arxiv:1701.01628.
 * 
 * and 
 *
 * I. Tal, A. Vardy ``How To Construct Polar Codes,''  IEEE Transactions on Information Theory, vol.
 * 59, no. 10, pp. 6562-6582, October 2013. 
 *
 * polarError is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or (at
 * your option) any later version.
 *
 * polarError is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with polarError; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */  
#ifndef LOWERBOUND_H
#define LOWERBOUND_H

#include <vector>
#include <fstream>
#include "TransformList.h"
#include "JointChannel.h"
#include "Channel.h"
#include "myfloat.h"

struct Results
{
    int aChannelIndex;
    int bChannelIndex; 
    int logLength;
    myfloat imjpErrorProbability; 
};

class LowerBound
{
public:
    LowerBound(Channel &ainitialChannel, int aaChannelAlphabetSize, int abChannelAlphabetSize);
    ~LowerBound();

    void generateTransformList(int aChannelIndex, int bChannelIndex, int aLogLength); 

    void computeLastSingleChannel(); 
    void computeLowerBound(int whatToPrint); 

    void printSelf(int whatToPrint = 0) ; 
    void printToFile(std::ofstream &filename) ; 
    void printResultsToFile(std::ofstream &filename) ; 

    myfloat getIMJP() const; 

private:
    Channel initialChannel; 
    int aChannelAlphabetSize;
    int bChannelAlphabetSize;
    
    int lastSingleChannelIndex;
    int lastSingleChannelLogLength;
    Channel lastSingleChannel;
    
    TransformList aChannelTransformList;
    TransformList bChannelTransformList;          
    bool areTransformListsLoaded;
    int logLength; 

    int logIndexOfFirstJointChannel;
        
    std::vector<myfloat> imjpErrorProbability;
    std::vector<Results> imjpResultsVector;

};

#endif /* LOWERBOUND_H */
