 /* JointChannel.h
 *
 * Copyright (C) 2018 Boaz Shuval <bshuval@campus.technion.ac.il> and Ido Tal <idotal@ieee.org> 
 *
 * This file is part of polarError. If you wish to use it, please cite: 
 * B. Shuval, I. Tal ``A Lower Bound on The Probability of Error of Polar Codes over BMS Channels,''
 * arxiv:1701.01628.
 * 
 * and 
 *
 * I. Tal, A. Vardy ``How To Construct Polar Codes,''  IEEE Transactions on Information Theory, vol.
 * 59, no. 10, pp. 6562-6582, October 2013. 
 *
 * polarError is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or (at
 * your option) any later version.
 *
 * polarError is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with polarError; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */    
#ifndef JOINT_CHANNEL_H
#define JOINT_CHANNEL_H

#include <armadillo>
#include <vector>
#include <utility>
#include "myfloat.h" 
#include "Channel.h" 

typedef std::pair<myfloat,myfloat> LRpair;


using namespace std; 

class JointChannel
{
    public:
        
        JointChannel(); 
        // Construct the symmetrized joint channel by applying - and + transforms on aChannel.
        // Upgrade the A and B marginal channels to have at most Ma and Mb letters, respectively.
        JointChannel(Channel &channel, int Ma, int Mb);

        // Construct a new joint channel from an old one. Apply transforms, and upgrade.
        JointChannel(JointChannel &jChannel, bool aPlus, bool bPlus, int Ma, int Mb);

        ~JointChannel();
        void printSelf(); 

        myfloat imjpErrorProb() const; 
        myfloat imlErrorProb() const; 


        myfloat probYaijGivenUa(int yaindex, int i, int j, int ua = 0) const; 
        myfloat probYaijUaLRGivenUb(int yaindex, int i, int j, int ua, int lrGtreq1Index, int signLLR = 1, int ub = 0) const; 


        /******************************************************************************************
        *                                  Transform functions                                   *
        ******************************************************************************************/
        // 
        //  The following illustration shows the various variables involved. 
        //  Note that (ua1,ub1,ua2,ub2) are "internal" -- they are not visible in the outward
        //  interface. 
        //
        //                                  +-------------------+
        //                              ua1 |                   |
        //       uaTop   -----(+)----------->     Top Copy      +----->  ya1,i1,j1
        //                     |            |                   |
        //                     |            |        of         +----->  ua1 = (uaTop XOR uaBot)
        //                     |        ub1 |                   |
        //       ubTop   ----- | --(+)------>       W_ab        +----->  signb1
        //                     |    |       |                   |
        //                     |    |       +-------------------+
        //                     |    |
        //                     |    |
        //                     |    |
        //                     |    |       +-------------------+
        //                     |    |   ua2 |                   |
        //       uaBot   ------*--- | ------>    Bottom Copy    +----->  ya2,i2,j2
        //                          |       |                   |
        //                          |       |        of         +----->  ua2 = uaBot
        //                          |   ub2 |                   |
        //       ubBot   -----------*------->       W_ab        +----->  signb2
        //                                  |                   |
        //                                  +-------------------+
        //
        // In all cases, ua, ub are the a-channel and b-channel inputs of the polarized channel. 
        // These differ per transform type: 
        //
        //    (*) -/-:    ua = uaTop,     ub = ubTop
        //    (*) -/+:    ua = uaTop,     ub = ubBot 
        //    (*) +/-:    ua = uaBot,     ub = ubTop 
        //    (*) +/+:    ua = uaBot,     ub = ubBot 
        //
        
        // For the polarized MM joint channel: 
        //      newYa = ((ya1,i1,j2),(ya2,i2,j2))
        //      newUa = ua       ( = uaTop )
        //      newYr = (signb1,signb2,uaBot)     
        //      newUb = ub       ( = ubTop )
        myfloat probPolarizedJointChannel_MM(int ya1, int i1, int j1, int signb1,
                                             int ya2, int i2, int j2, int signb2, 
                                             int ua, int uaBot, int ub ) const; 
            
        // For the polarized MP joint channel: 
        //      newYa = ((ya1,i1,j2),(ya2,i2,j2))
        //      newUa = ua       ( = uaTop )
        //      newYr = (signb1,signb2,uaBot, ubTop)     
        //      newUb = ub       ( = ubBot )
        myfloat probPolarizedJointChannel_MP(int ya1, int i1, int j1, int signb1,
                                             int ya2, int i2, int j2, int signb2, 
                                             int ua, int uaBot, int ubTop, int ub ) const; 

        // For the polarized PM joint channel: 
        //      newYa = ((ya1,i1,j2),(ya2,i2,j2), uaTop)
        //      newUa = ua       ( = uaBot )
        //      newYr = (signb1,signb2)     
        //      newUb = ub       ( = ubTop )
        myfloat probPolarizedJointChannel_PM(int ya1, int i1, int j1, int signb1,
                                             int ya2, int i2, int j2, int signb2, 
                                             int uaTop, int ua, int ub ) const; 


        // For the polarized PP joint channel: 
        //      newYa = ((ya1,i1,j2),(ya2,i2,j2), uaTop)
        //      newUa = ua       ( = uaBot )
        //      newYr = (signb1,signb2,ubTop)     
        //      newUb = ub       ( = ubBot )
        myfloat probPolarizedJointChannel_PP(int ya1, int i1, int j1, int signb1,
                                             int ya2, int i2, int j2, int signb2, 
                                             int uaTop, int ua, int ubTop, int ub ) const; 




    private:
        Channel channelA;
        Channel channelB;
        arma::Cube<myfloat> yaijGivenUaZero; 

        int lrsGtreq1InChannelBCount;
        int outputSizeOfChannelA;

        vector<myfloat> LRgtreq1InChannelA; 
        vector<myfloat> LRgtreq1InChannelB; 
        vector<LRpair> LRpairGtreq1vecInChannelA; 
        vector<LRpair> LRpairGtreq1vecInChannelB; 

        bool calcLRpairNeighborsAndCoefficients(const LRpair &inputLRpair,
                const vector<LRpair> &LRpairGtreq1InUpgradedChannel,
                int  &leftLRpairIndex, myfloat  &leftLRpairCoefficient,
                int &rightLRpairIndex, myfloat &rightLRpairCoefficient);
        void initializePrivateVars();
        
        void splitProbIntoJointChannel(myfloat probYa, LRpair lrAPairNewYa, LRpair lrBPairUaZero, LRpair lrBPairUaOne);
        void polarizeAndUpgradeSymbols(JointChannel &jChannel, int y1even, int i1, int j1, int 
                y2even, int i2, int j2, bool aPlus, bool bPlus);

        myfloat &probYaijGivenUa(int yaindex, int i, int j, int ua = 0); 

        /******************************************************************************************
        *                                  AUXILIARY FUNCTIONS                                   *
        ******************************************************************************************/

        myfloat computeAlpha(myfloat i, myfloat WaNewYa, myfloat WbNewYaUaSumUb) const;
        myfloat signYa(int yaindex) const; 

        myfloat computeLRfactorFromPair(LRpair lrPair, int u) const; 

        LRpair lrMinusPair(LRpair lrPair1, LRpair lrPair2) const; 
        LRpair lrPlusPair(LRpair lrPair1, LRpair lrPair2, int u, LRpair fallbackLR) const; 
        LRpair makeGtreq1(LRpair lrPair) const; 

        myfloat computeLRfromPair(LRpair lrPair) const; 
        myfloat computeLRGtreq1fromPair(LRpair lrPair) const; 

        LRpair signedLRPair(LRpair lrPair, int signLLR) const; 
};

#endif // JOINT_CHANNEL_H



