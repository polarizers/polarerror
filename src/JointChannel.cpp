 /* JointChannel.cpp
 *
 * Copyright (C) 2018 Boaz Shuval <bshuval@campus.technion.ac.il> and Ido Tal <idotal@ieee.org> 
 *
 * This file is part of polarError. If you wish to use it, please cite: 
 * B. Shuval, I. Tal ``A Lower Bound on The Probability of Error of Polar Codes over BMS Channels,''
 * arxiv:1701.01628.
 * 
 * and 
 *
 * I. Tal, A. Vardy ``How To Construct Polar Codes,''  IEEE Transactions on Information Theory, vol.
 * 59, no. 10, pp. 6562-6582, October 2013. 
 *
 * polarError is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or (at
 * your option) any later version.
 *
 * polarError is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with polarError; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */    
#include "JointChannel.h"
#include "assert.h"
#include <iostream>
#include <iomanip>
#include <algorithm>
#include "KahanSum.h"

const myfloat mindenom = 1e-13;
const myfloat smallestPositiveProb = 1e-15;
const myfloat verySmallProbability = 1e-300;

JointChannel::JointChannel()
{
}

JointChannel::JointChannel(Channel &channel, int Ma, int Mb)
{
    int y1, y2;
    LRpair lrBPairUaZero, lrBPairUaOne, lrAPairNewYa;
    myfloat prob1GivenU1Zero, prob1GivenU1One, prob2GivenU2Zero, prob2GivenU2One;
    myfloat probYaGivenUaZero;
    myfloat probYaGivenUaOne;
    myfloat probYa;


    (channel.minusTransform()).upgrade(channelA, Ma);
    (channel.plusTransform()).upgrade(channelB, Mb);

    initializePrivateVars();

    for ( y1 = 0; y1 < channel.getOutputAlphabetSize(); y1 += 1 )
    {
        for ( y2 = 0; y2 < channel.getOutputAlphabetSize(); y2 += 1 )
        {
            prob1GivenU1Zero = channel.getProbOutputGivenInput(y1, 0);
            prob1GivenU1One = channel.getProbOutputGivenInput(y1, 1);

            prob2GivenU2Zero = channel.getProbOutputGivenInput(y2, 0);
            prob2GivenU2One = channel.getProbOutputGivenInput(y2, 1);

            probYaGivenUaZero = 0.5*(prob1GivenU1Zero*prob2GivenU2Zero + prob1GivenU1One*prob2GivenU2One);
            probYaGivenUaOne = 0.5*(prob1GivenU1Zero*prob2GivenU2One + prob1GivenU1One*prob2GivenU2Zero);
            
            probYa = 0.5*(probYaGivenUaZero + probYaGivenUaOne);  

            lrAPairNewYa = make_pair(probYaGivenUaZero, probYaGivenUaOne); 
            assert( probYa >= 0.0 );

            if ( probYaGivenUaZero > smallestPositiveProb )
            {
                lrBPairUaZero = makeGtreq1( make_pair(0.5*prob1GivenU1Zero*prob2GivenU2Zero, 0.5*prob1GivenU1One*prob2GivenU2One) );
            }
            else // map to the highest B-channel LLR, an a-channel symbol
                //that has zero probability of appearing, if ua = 0
            {
                lrBPairUaZero = LRpairGtreq1vecInChannelB.at(0);  
            }

            if ( probYaGivenUaOne > smallestPositiveProb )
            {
                lrBPairUaOne = makeGtreq1( make_pair(0.5*prob1GivenU1Zero*prob2GivenU2One, 0.5*prob1GivenU1One*prob2GivenU2Zero) );
            }
            else // map to the highest B-channel LLR, an a-channel symbol
                //that has zero probability of appearing, if ua = 1
            {
                lrBPairUaOne = LRpairGtreq1vecInChannelB.at(0);  
            }
            
            assert(!isnan(probYa));
            splitProbIntoJointChannel(probYa, lrAPairNewYa, lrBPairUaZero, lrBPairUaOne);
            
        }
    }
}

JointChannel::JointChannel(JointChannel &jChannel, bool aPlus, bool bPlus, int Ma, int Mb)
{
    int y1even, i1, j1, y2even, i2, j2;


    // create channelA
    if ( aPlus == true )
    {
        (jChannel.channelA.plusTransform()).upgrade(channelA, Ma);
    }
    else
    {
        (jChannel.channelA.minusTransform()).upgrade(channelA, Ma);
    }

    // create channelB
    if ( bPlus == true )
    {
        (jChannel.channelB.plusTransform()).upgrade(channelB, Mb);
    }
    else
    {
        (jChannel.channelB.minusTransform()).upgrade(channelB, Mb);
    }

    initializePrivateVars();

    for ( y1even = 0; y1even < jChannel.outputSizeOfChannelA; y1even += 1 )
    {
        for ( i1 = 0; i1 < jChannel.lrsGtreq1InChannelBCount; i1++ )
        {
            for ( j1 = 0; j1 < jChannel.lrsGtreq1InChannelBCount; j1++ )
            {
                for ( y2even = 0; y2even < jChannel.outputSizeOfChannelA; y2even += 1 )
                {
                    for ( i2 = 0; i2 < jChannel.lrsGtreq1InChannelBCount; i2++ )
                    {
                        for ( j2 = 0; j2 < jChannel.lrsGtreq1InChannelBCount; j2++ )
                        {
                            polarizeAndUpgradeSymbols(jChannel, y1even, i1, j1,
                                    y2even, i2, j2, aPlus, bPlus);
                        }
                    }
                }
            }
        }
    }
}

void JointChannel::initializePrivateVars()
{

    lrsGtreq1InChannelBCount = channelB.getOutputAlphabetSize()/2;
    outputSizeOfChannelA = channelA.getOutputAlphabetSize();

    LRpairGtreq1vecInChannelB = channelB.getLRpairvec();
    LRpairGtreq1vecInChannelA = channelA.getLRpairvec();

    yaijGivenUaZero.zeros(lrsGtreq1InChannelBCount, lrsGtreq1InChannelBCount, outputSizeOfChannelA); 
}

JointChannel::~JointChannel()
{

}

bool JointChannel::calcLRpairNeighborsAndCoefficients( const LRpair &inputLRpair,
        const vector<LRpair> &LRpairGtreq1InUpgradedChannel,
        int  &leftLRpairIndex, myfloat  &leftLRpairCoefficient,
        int &rightLRpairIndex, myfloat &rightLRpairCoefficient)
{
    myfloat aInput, bInput; 
    
    aInput = inputLRpair.first; 
    bInput = inputLRpair.second; 
    assert(aInput > 0.0 || bInput > 0.0);
    assert(aInput >= 0.0 && bInput >= 0.0); 
    assert (LRpairGtreq1InUpgradedChannel.empty() == false );

    if (LRpairGtreq1InUpgradedChannel.size() == 1)
    {
        leftLRpairIndex = 0;
        leftLRpairCoefficient = 1.0;

        rightLRpairIndex = -100; // garbage
        rightLRpairCoefficient = -100.0; // grabage
        return true;
    }

    // The LRpairGtreq1InUpgradedChannel vector is sorted by decreasing LR. We now search for the
    // first LR that is *not* (strictly) greater than that of inputLRpair. 
    rightLRpairIndex = 0; 
    for (const LRpair &lrPair : LRpairGtreq1InUpgradedChannel )
    {
        if ( lrPair.first * bInput > lrPair.second * aInput)
        {
            rightLRpairIndex++; 
        }
        else
        {
            break;
        }
    }

    // If we are on the wrong side of an extreme value, fix.
    if ( rightLRpairIndex ==  0 )
    {
        rightLRpairIndex++;
        leftLRpairIndex = rightLRpairIndex - 1;
        rightLRpairCoefficient = 0.0;
        leftLRpairCoefficient  = 1.0; 
        return false; 
    }

    if ( rightLRpairIndex == LRpairGtreq1InUpgradedChannel.size() )
    {
        rightLRpairIndex--;
        leftLRpairIndex = rightLRpairIndex - 1;
        rightLRpairCoefficient = 1.0;
        leftLRpairCoefficient  = 0.0; 
        return false; 
    }
    
    leftLRpairIndex = rightLRpairIndex - 1; 

    myfloat LRleft, LRright, denom;
    denom = 0.0; 

    LRleft  = computeLRGtreq1fromPair(LRpairGtreq1InUpgradedChannel.at(leftLRpairIndex)); 
    LRright = computeLRGtreq1fromPair(LRpairGtreq1InUpgradedChannel.at(rightLRpairIndex)); 
    
    if ( LRpairGtreq1InUpgradedChannel.at(leftLRpairIndex).second == 0.0)
    {
        rightLRpairCoefficient = ( (1+LRright) * bInput )    /(aInput + bInput);
        leftLRpairCoefficient  = ( aInput - LRright * bInput)/(aInput + bInput);
    }
    else
    {
        denom = LRleft - LRright;
        assert( denom > 0.0 );

        if (denom < mindenom)
        {
            leftLRpairCoefficient = 1.0; 
        }
        else
        {
            leftLRpairCoefficient  = ( (1+LRleft )/(aInput + bInput) )/denom * (aInput - LRright * bInput);
        }
        rightLRpairCoefficient = 1.0 - leftLRpairCoefficient; 
    }
    
    //Fix for numerical errors
    if (rightLRpairCoefficient > 1.0)
    {
        assert(leftLRpairCoefficient < smallestPositiveProb);
        rightLRpairCoefficient = 1.0; 
        leftLRpairCoefficient  = 0.0;
    }
    if (leftLRpairCoefficient > 1.0)
    {
        assert(rightLRpairCoefficient < smallestPositiveProb);
        rightLRpairCoefficient = 0.0;
        leftLRpairCoefficient  = 1.0;
    }
    
    if (rightLRpairCoefficient < 0.0)
    {
        assert(leftLRpairCoefficient > 1.0 - smallestPositiveProb);
        rightLRpairCoefficient = 0.0; 
        leftLRpairCoefficient  = 1.0;
    }
    if (leftLRpairCoefficient < 0.0)
    {
        assert(rightLRpairCoefficient > 1.0 - smallestPositiveProb);
        rightLRpairCoefficient = 1.0;
        leftLRpairCoefficient  = 0.0;
    }

    return false;

}


void JointChannel::printSelf() 
{
   int yaindex, i, j;

   cout << "*** The upgraded A channel is" << endl;
   channelA.printSelf();

   cout << "*** The upgraded B channel is" << endl;
   channelB.printSelf();

   cout << "*** The joint channel is" << endl;
   for(yaindex=0; yaindex < outputSizeOfChannelA; yaindex++) 
   {
       for(i = 0; i < lrsGtreq1InChannelBCount; i++)
       {
           for(j = 0; j < lrsGtreq1InChannelBCount; j++)
           {
               cout << "ya = " << yaindex;
               cout << ", i = " << i << ", j = " << j;
               cout << ", W(ya^{i,j}|0) = " << setw(12) << left <<  probYaijGivenUa(yaindex,i,j,0); 
               cout << ", W(ya^{i,j}) = " << setw(12) << left << (probYaijGivenUa(yaindex,i,j,0)+probYaijGivenUa(yaindex,i,j,1))*0.5; 
               cout << ", LRa = " << computeLRfromPair(signedLRPair(LRpairGtreq1vecInChannelA.at(yaindex/2), signYa(yaindex)));
               cout << ", LRi = " << computeLRfromPair(LRpairGtreq1vecInChannelB.at(i));
               cout << ", LRj = " << computeLRfromPair(LRpairGtreq1vecInChannelB.at(j));
               cout << endl;
           }
       }
   }

}

myfloat JointChannel::imjpErrorProb() const
{
    KahanSum errProb;
    int yaindex;
    int i, j, ij;
    int signLLR; 
    int ua, ub; 

    myfloat pa0, pa1; 
    myfloat pb0, pb1;

    int uaDecision, ubDecision;

    errProb.startNewSum(); 
    
    for ( yaindex = 0 ; yaindex < outputSizeOfChannelA ; yaindex++)
    {
        for (i = 0; i < lrsGtreq1InChannelBCount ; i++)
        {
            for (j = 0; j < lrsGtreq1InChannelBCount ; j++)
            {
                pa0 = probYaijGivenUa(yaindex, i, j, 0)*computeLRfactorFromPair(LRpairGtreq1vecInChannelB.at(i),0);  
                pa1 = probYaijGivenUa(yaindex, i, j, 1)*computeLRfactorFromPair(LRpairGtreq1vecInChannelB.at(j),0); 

                if ( pa0 - pa1 >= -smallestPositiveProb )
                {
                    uaDecision = 0;
                }
                else
                {
                    uaDecision = 1;
                }
                   
                for (ua = 0 ; ua < 2 ; ua ++ )
                {
                    ij = ( (ua == 0) ? i : j ); 

                    for(signLLR = -1; signLLR < 2; signLLR += 2)
                    {
                        pb0 = probYaijUaLRGivenUb(yaindex,i,j,ua,ij,signLLR, 0);
                        pb1 = probYaijUaLRGivenUb(yaindex,i,j,ua,ij,signLLR, 1);

                        if ( pb0 - pb1 >= -smallestPositiveProb )
                        {
                            ubDecision = 0;
                        }
                        else
                        {
                            ubDecision = 1;
                        }

                        for (ub = 0; ub < 2; ub ++)
                        {
                            if (uaDecision != ua || ubDecision != ub)
                            {
                                errProb.insert(probYaijUaLRGivenUb(yaindex, i, j, ua, ij, signLLR, ub)); 
                            }
                        }
                    }
                }
            }
        }
    }
    return errProb.calcSum()/2.0;
}


myfloat JointChannel::imlErrorProb() const
{
    myfloat errProb;
    int yaindex;
    int i, j, ij;
    int signLLR; 
    int ua, ub; 

    myfloat pa0, pa1; 
    myfloat pb0, pb1;

    int uaDecision, ubDecision;

    errProb = 0.0; 
    for ( yaindex = 0 ; yaindex < outputSizeOfChannelA ; yaindex++)
    {
        for (i = 0; i < lrsGtreq1InChannelBCount ; i++)
        {
            for (j = 0; j < lrsGtreq1InChannelBCount ; j++)
            {
                pa0 = probYaijGivenUa(yaindex, i, j, 0); 
                pa1 = probYaijGivenUa(yaindex, i, j, 1); 

                if ( pa0 - pa1 >= -smallestPositiveProb )
                {
                    uaDecision = 0;
                }
                else
                {
                    uaDecision = 1;
                }
                   
                for (ua = 0 ; ua < 2 ; ua ++ )
                {
                    ij = ( (ua == 0) ? i : j ); 

                    for(signLLR = -1; signLLR < 2; signLLR += 2)
                    {
                        pb0 = probYaijUaLRGivenUb(yaindex,i,j,ua,ij,signLLR, 0);
                        pb1 = probYaijUaLRGivenUb(yaindex,i,j,ua,ij,signLLR, 1);

                        if ( pb0 - pb1 >= -smallestPositiveProb )
                        {
                            ubDecision = 0;
                        }
                        else
                        {
                            ubDecision = 1;
                        }

                        for (ub = 0; ub < 2; ub ++)
                        {
                            if (uaDecision != ua || ubDecision != ub)
                            {
                                errProb += probYaijUaLRGivenUb(yaindex, i, j, ua, ij, signLLR, ub); 
                            }
                        }
                    }
                }
            }
        }
    }
            
    return errProb/2.0;
}

myfloat JointChannel::probYaijGivenUa(int yaindex, int i, int j, int ua) const
{
    assert(yaindex >= 0 && yaindex < outputSizeOfChannelA); 
    assert(i >= 0 && i < lrsGtreq1InChannelBCount);
    assert(j >= 0 && j < lrsGtreq1InChannelBCount);

    if (ua == 0)
    {
        return yaijGivenUaZero(i,j,yaindex);
    }
    else // ua == 1, so swap i and j
    {
        if (yaindex %2 == 0)
        {
            return yaijGivenUaZero(j,i,yaindex+1); 
        }
        else 
        {
            return yaijGivenUaZero(j,i,yaindex-1);
        }
    }
}

myfloat &JointChannel::probYaijGivenUa(int yaindex, int i, int j, int ua) 
{
    assert(yaindex >= 0 && yaindex < outputSizeOfChannelA); 
    assert(i >= 0 && i < lrsGtreq1InChannelBCount);
    assert(j >= 0 && j < lrsGtreq1InChannelBCount);

    if (ua == 0)
    {
        return yaijGivenUaZero(i,j,yaindex);
    }
    else // ua == 1, so swap i and j
    {
        if (yaindex %2 == 0)
        {
            return yaijGivenUaZero(j,i,yaindex+1);
        }
        else 
        {
            return yaijGivenUaZero(j,i,yaindex-1);
        }
    }
}


void JointChannel::splitProbIntoJointChannel(myfloat probYa, LRpair lrAPairNewYa,
        LRpair lrBPairUaZero, LRpair lrBPairUaOne)
{
    myfloat iCoefficients[2];
    myfloat jCoefficients[2];
    myfloat yaCoefficients[2];

    int iLocations[2];
    int jLocations[2];
    int yaLocations[2];
    
    bool singlei, singlej, singleYa;

    int yaIndex, i, j ;
    LRpair lrAPairTarget ;
    myfloat targetProb, multByThreeCoefficient;

    if(probYa == 0.0)
    {
        return;
    }
    assert(lrAPairNewYa.first > 0.0  || lrAPairNewYa.second > 0.0);
    assert(lrAPairNewYa.first >= 0.0 && lrAPairNewYa.second >= 0.0);

    if(lrAPairNewYa.first < lrAPairNewYa.second)
    {
        swap(lrBPairUaZero, lrBPairUaOne); 
        lrAPairNewYa = makeGtreq1(lrAPairNewYa); 
    }

    singlei = calcLRpairNeighborsAndCoefficients(lrBPairUaZero, LRpairGtreq1vecInChannelB, iLocations[0],
            iCoefficients[0], iLocations[1], iCoefficients[1]);

    singlej = calcLRpairNeighborsAndCoefficients(lrBPairUaOne, LRpairGtreq1vecInChannelB, jLocations[0], 
            jCoefficients[0], jLocations[1], jCoefficients[1]);

    singleYa = calcLRpairNeighborsAndCoefficients(lrAPairNewYa, LRpairGtreq1vecInChannelA, yaLocations[0], 
            yaCoefficients[0], yaLocations[1], yaCoefficients[1]);

    // left = 0, right = 1
    //
    // The check "&& (singlei == false || iChoice == 0)" is for the
    // edge case in which the upgraded channel contains only a pair of
    // output symbols. 

    for (int yaChoice = 0; yaChoice < 2 && (singleYa == false || yaChoice == 0); ++yaChoice)
    {
        for( int iChoice = 0; iChoice < 2 && (singlei == false || iChoice == 0); iChoice++ )
        {
            for (int jChoice = 0; jChoice < 2 && (singlej == false || jChoice == 0); ++jChoice)
            {
                yaIndex = 2*yaLocations[yaChoice];
                i = iLocations[iChoice];
                j = jLocations[jChoice];
                lrAPairTarget = LRpairGtreq1vecInChannelA.at( yaLocations[yaChoice] );

                multByThreeCoefficient = iCoefficients[iChoice] * jCoefficients[jChoice] * yaCoefficients[yaChoice];                          

                for (int ua = 0; ua < 2; ua++) 
                {
                    targetProb = probYa * computeLRfactorFromPair(lrAPairTarget, ua) * multByThreeCoefficient; 
                    probYaijGivenUa(yaIndex,i,j,ua)  += targetProb;
                }

            }
        }

    }
}

void JointChannel::polarizeAndUpgradeSymbols(JointChannel &jChannel, int ya1, int i1, int j1, int
                ya2, int i2, int j2, bool aPlus, bool bPlus)
{
    int ya1_LRIndex, ya2_LRIndex; 
    myfloat prob_ya1i1j1_givenUa1Zero, prob_ya1i1j1_givenUa1One; 
    myfloat prob_ya2i2j2_givenUa2Zero, prob_ya2i2j2_givenUa2One; 
    LRpair fallbackLR_a, fallbackLR_b;
    
    myfloat prob_ya1i1j1_ya2i2j2_givenUaZero, prob_ya1i1j1_ya2i2j2_givenUaOne;
    myfloat denom;
    myfloat probNewYa;
    
    LRpair lrPair1,lrPair2;
    LRpair lrAPairNewYa, lrBPairUaZero, lrBPairUaOne;
    
    myfloat WaNewYaGivenUaZero, WaNewYaGivenUaOne;
    vector<myfloat> WbNewYaUaZeroSumUb, WbNewYaUaOneSumUb; 
    myfloat alpha_i, alpha_j, alphaij;
    vector<LRpair>  lrBPairForYaUaZero, lrBPairForYaUaOne; 
    int numOfIJ;

    ya1_LRIndex = ya1/2;
    ya2_LRIndex = ya2/2;
                                                                   //ua
    prob_ya1i1j1_givenUa1Zero = jChannel.probYaijGivenUa(ya1, i1, j1, 0);
    prob_ya1i1j1_givenUa1One  = jChannel.probYaijGivenUa(ya1, i1, j1, 1);

    prob_ya2i2j2_givenUa2Zero = jChannel.probYaijGivenUa(ya2, i2, j2, 0);
    prob_ya2i2j2_givenUa2One  = jChannel.probYaijGivenUa(ya2, i2, j2, 1);

    fallbackLR_a = jChannel.LRpairGtreq1vecInChannelA.at(0); 
    fallbackLR_b = jChannel.LRpairGtreq1vecInChannelB.at(0); 
    
    if (aPlus == false)
    {
        // Minus transform on "a" channel, thus, Ua = U1 + U2, where U1 and U2 are the inputs to
        // the "a" part of the two copies of the original channel
        // The factor 0.5 is due to marginalizing over U2. 
        prob_ya1i1j1_ya2i2j2_givenUaZero = 0.5*(prob_ya1i1j1_givenUa1Zero * prob_ya2i2j2_givenUa2Zero 
                                                + prob_ya1i1j1_givenUa1One * prob_ya2i2j2_givenUa2One);
        prob_ya1i1j1_ya2i2j2_givenUaOne  = 0.5*(prob_ya1i1j1_givenUa1Zero * prob_ya2i2j2_givenUa2One 
                                                + prob_ya1i1j1_givenUa1One * prob_ya2i2j2_givenUa2Zero);

        denom  = (prob_ya1i1j1_ya2i2j2_givenUaZero + prob_ya1i1j1_ya2i2j2_givenUaOne);  

        // newYa is a target symbol, one in a set of symbols gotten from the upgrade-couple operation.
        // All such symbols have the same da.
        probNewYa = 0.5 * denom; 

        lrPair1 = signedLRPair(jChannel.LRpairGtreq1vecInChannelA.at(ya1_LRIndex), signYa(ya1));
        lrPair2 = signedLRPair(jChannel.LRpairGtreq1vecInChannelA.at(ya2_LRIndex), signYa(ya2));
        lrAPairNewYa = lrMinusPair(lrPair1,lrPair2); 
        
        WaNewYaGivenUaZero = probNewYa*computeLRfactorFromPair(lrAPairNewYa, 0); 
        WaNewYaGivenUaOne  = probNewYa*computeLRfactorFromPair(lrAPairNewYa, 1); 

        if (bPlus == false)
        {
            numOfIJ = 2;

            lrBPairForYaUaZero.reserve(numOfIJ);                                
            lrBPairForYaUaOne.reserve(numOfIJ);                                
            WbNewYaUaZeroSumUb.reserve(numOfIJ);                                
            WbNewYaUaOneSumUb.reserve(numOfIJ);                                

            for(int uaBot = 0; uaBot <2 ; uaBot++)
            {
                // ua == 0 
                lrPair1 = jChannel.LRpairGtreq1vecInChannelB.at((uaBot % 2) == 0 ? i1 : j1);
                lrPair2 = jChannel.LRpairGtreq1vecInChannelB.at(   uaBot    == 0 ? i2 : j2); 
                
                lrBPairForYaUaZero.push_back( makeGtreq1(lrMinusPair(lrPair1,lrPair2))  ); 

                // Sum over Ub = Sum over positive and negative D-values, with ub = 0.
                WbNewYaUaZeroSumUb.push_back(     //               signb1       signb2 ua        ub
                    jChannel.probPolarizedJointChannel_MM(ya1,i1,j1, 1 ,ya2,i2,j2, 1 ,  0, uaBot, 0) + 
                    jChannel.probPolarizedJointChannel_MM(ya1,i1,j1, 1 ,ya2,i2,j2,-1 ,  0, uaBot, 0) +
                    jChannel.probPolarizedJointChannel_MM(ya1,i1,j1,-1 ,ya2,i2,j2, 1 ,  0, uaBot, 0) + 
                    jChannel.probPolarizedJointChannel_MM(ya1,i1,j1,-1 ,ya2,i2,j2,-1 ,  0, uaBot, 0));

                // ua == 1
                lrPair1 = jChannel.LRpairGtreq1vecInChannelB.at( ((1+uaBot) % 2) == 0 ? i1 : j1 );
                lrPair2 = jChannel.LRpairGtreq1vecInChannelB.at(      uaBot      == 0 ? i2 : j2 ); 

                lrBPairForYaUaOne.push_back( makeGtreq1(lrMinusPair(lrPair1,lrPair2))  ); 

                WbNewYaUaOneSumUb.push_back(      //               signb1        signb2 ua        ub
                    jChannel.probPolarizedJointChannel_MM(ya1,i1,j1, 1 ,ya2,i2,j2, 1 ,  1, uaBot, 0) + 
                    jChannel.probPolarizedJointChannel_MM(ya1,i1,j1, 1 ,ya2,i2,j2,-1 ,  1, uaBot, 0) +
                    jChannel.probPolarizedJointChannel_MM(ya1,i1,j1,-1 ,ya2,i2,j2, 1 ,  1, uaBot, 0) + 
                    jChannel.probPolarizedJointChannel_MM(ya1,i1,j1,-1 ,ya2,i2,j2,-1 ,  1, uaBot, 0));
            }

            for( int i = 0; i < numOfIJ ; i++)
            {
                for( int j = 0; j < numOfIJ ; j++)
                {
                    lrBPairUaZero = lrBPairForYaUaZero.at(i);
                    lrBPairUaOne  = lrBPairForYaUaOne.at(j);

                    alpha_i = computeAlpha(i, WaNewYaGivenUaZero, WbNewYaUaZeroSumUb.at(i)); 
                    alpha_j = computeAlpha(j, WaNewYaGivenUaOne , WbNewYaUaOneSumUb.at(j) ); 
                    alphaij = alpha_i*alpha_j;
                    splitProbIntoJointChannel(alphaij * probNewYa, lrAPairNewYa, lrBPairUaZero, lrBPairUaOne);                  
                }
            }
        }
        else // bPlus == true
        {
            numOfIJ = 4;
            lrBPairForYaUaZero.reserve(numOfIJ); 
            lrBPairForYaUaOne.reserve(numOfIJ); 
            WbNewYaUaZeroSumUb.reserve(numOfIJ);                                
            WbNewYaUaOneSumUb.reserve(numOfIJ);                                

            for(int uaBot = 0 ; uaBot < 2 ; uaBot ++)
            {
                /************
                *  ua == 0  *
                ************/
                lrPair1 = jChannel.LRpairGtreq1vecInChannelB.at( (uaBot % 2) == 0 ? i1 : j1 );
                lrPair2 = jChannel.LRpairGtreq1vecInChannelB.at(    uaBot    == 0 ? i2 : j2 ); 
                
                lrBPairForYaUaZero.push_back( makeGtreq1(lrPlusPair(lrPair1,lrPair2,0,fallbackLR_b)) ); 
                lrBPairForYaUaZero.push_back( makeGtreq1(lrPlusPair(lrPair1,lrPair2,1,fallbackLR_b)) ); 
                
                // Sum over Ub = Sum over positive and negative D-values, with ub = 0.
                WbNewYaUaZeroSumUb.push_back(      //             signb1       signb2 ua      ubTop ub
                    jChannel.probPolarizedJointChannel_MP(ya1,i1,j1, 1 ,ya2,i2,j2, 1 , 0, uaBot, 0, 0) + 
                    jChannel.probPolarizedJointChannel_MP(ya1,i1,j1,-1 ,ya2,i2,j2,-1 , 0, uaBot, 0, 0) +
                    jChannel.probPolarizedJointChannel_MP(ya1,i1,j1, 1 ,ya2,i2,j2,-1 , 0, uaBot, 1, 0) +
                    jChannel.probPolarizedJointChannel_MP(ya1,i1,j1,-1 ,ya2,i2,j2, 1 , 0, uaBot, 1, 0));

                WbNewYaUaZeroSumUb.push_back(      //             signb1       signb2 ua      ubTop ub
                    jChannel.probPolarizedJointChannel_MP(ya1,i1,j1, 1 ,ya2,i2,j2, 1 , 0, uaBot, 1, 0) + 
                    jChannel.probPolarizedJointChannel_MP(ya1,i1,j1,-1 ,ya2,i2,j2,-1 , 0, uaBot, 1, 0) +
                    jChannel.probPolarizedJointChannel_MP(ya1,i1,j1, 1 ,ya2,i2,j2,-1 , 0, uaBot, 0, 0) +
                    jChannel.probPolarizedJointChannel_MP(ya1,i1,j1,-1 ,ya2,i2,j2, 1 , 0, uaBot, 0, 0)); 

                
                /************
                *  ua == 1  *
                ************/
                lrPair1 = jChannel.LRpairGtreq1vecInChannelB.at( ((1+uaBot) % 2) == 0 ? i1 : j1 );
                lrPair2 = jChannel.LRpairGtreq1vecInChannelB.at(    uaBot        == 0 ? i2 : j2 ); 
                
                lrBPairForYaUaOne.push_back( makeGtreq1(lrPlusPair(lrPair1,lrPair2,0,fallbackLR_b)) ); 
                lrBPairForYaUaOne.push_back( makeGtreq1(lrPlusPair(lrPair1,lrPair2,1,fallbackLR_b)) ); 

                WbNewYaUaOneSumUb.push_back(      //             signb1       signb2 ua      ubTop ub
                    jChannel.probPolarizedJointChannel_MP(ya1,i1,j1, 1 ,ya2,i2,j2, 1 , 1, uaBot, 0, 0) + 
                    jChannel.probPolarizedJointChannel_MP(ya1,i1,j1,-1 ,ya2,i2,j2,-1 , 1, uaBot, 0, 0) +
                    jChannel.probPolarizedJointChannel_MP(ya1,i1,j1, 1 ,ya2,i2,j2,-1 , 1, uaBot, 1, 0) +
                    jChannel.probPolarizedJointChannel_MP(ya1,i1,j1,-1 ,ya2,i2,j2, 1 , 1, uaBot, 1, 0));

                WbNewYaUaOneSumUb.push_back(      //             signb1       signb2 ua      ubTop ub
                    jChannel.probPolarizedJointChannel_MP(ya1,i1,j1, 1 ,ya2,i2,j2, 1 , 1, uaBot, 1, 0) + 
                    jChannel.probPolarizedJointChannel_MP(ya1,i1,j1,-1 ,ya2,i2,j2,-1 , 1, uaBot, 1, 0) +
                    jChannel.probPolarizedJointChannel_MP(ya1,i1,j1, 1 ,ya2,i2,j2,-1 , 1, uaBot, 0, 0) +
                    jChannel.probPolarizedJointChannel_MP(ya1,i1,j1,-1 ,ya2,i2,j2, 1 , 1, uaBot, 0, 0));

            }

            for( int i = 0; i < numOfIJ ; i++)
            {
                for( int j = 0; j < numOfIJ ; j++)
                {
                    lrBPairUaZero = lrBPairForYaUaZero.at(i);
                    lrBPairUaOne  = lrBPairForYaUaOne.at(j);

                    alpha_i = computeAlpha(i, WaNewYaGivenUaZero, WbNewYaUaZeroSumUb.at(i)); 
                    alpha_j = computeAlpha(j, WaNewYaGivenUaOne , WbNewYaUaOneSumUb.at(j) ); 
                    alphaij = alpha_i*alpha_j;
                    splitProbIntoJointChannel(alphaij * probNewYa, lrAPairNewYa, lrBPairUaZero, lrBPairUaOne);
                }
            } 
        }
    }
    else // aPlus == true
    {
        for (int uaTop = 0; uaTop < 2; uaTop++)
        {
            lrBPairForYaUaZero.clear();
            lrBPairForYaUaOne.clear();
            WbNewYaUaZeroSumUb.clear();
            WbNewYaUaOneSumUb.clear();
            
            // Plus transform on a-channel. Thus, we treat the cases uaTop = 0 and uaTop = 1 
            // differently. Recall that ua1 = uaTop XOR ua.
            if(uaTop == 0)
            {
                prob_ya1i1j1_ya2i2j2_givenUaZero = 0.5*prob_ya1i1j1_givenUa1Zero*prob_ya2i2j2_givenUa2Zero;
                prob_ya1i1j1_ya2i2j2_givenUaOne  = 0.5*prob_ya1i1j1_givenUa1One *prob_ya2i2j2_givenUa2One;
            }
            else // uaTop == 1
            { 
                prob_ya1i1j1_ya2i2j2_givenUaZero = 0.5*prob_ya1i1j1_givenUa1One *prob_ya2i2j2_givenUa2Zero;
                prob_ya1i1j1_ya2i2j2_givenUaOne  = 0.5*prob_ya1i1j1_givenUa1Zero*prob_ya2i2j2_givenUa2One;
            }

            denom  = (prob_ya1i1j1_ya2i2j2_givenUaZero + prob_ya1i1j1_ya2i2j2_givenUaOne);  

            // newYa is a target symbol, one in a set of symbols gotten from the upgrade-couple operation.
            // All such symbols have the same da.
            probNewYa = 0.5 * denom; 
            lrPair1 = signedLRPair(jChannel.LRpairGtreq1vecInChannelA.at(ya1_LRIndex), signYa(ya1));
            lrPair2 = signedLRPair(jChannel.LRpairGtreq1vecInChannelA.at(ya2_LRIndex), signYa(ya2));
            lrAPairNewYa = lrPlusPair(lrPair1,lrPair2,uaTop, fallbackLR_a); 

            WaNewYaGivenUaZero = probNewYa*computeLRfactorFromPair(lrAPairNewYa, 0); 
            WaNewYaGivenUaOne  = probNewYa*computeLRfactorFromPair(lrAPairNewYa, 1); 

            if (bPlus == false)
            {
                // In this case, lrBPairUaZero and lrBPairUaOne are uniquely determined. Therefore,
                // there is no need to compute alphaij. 
                //
                /************
                 *  ua == 0  *
                 ************/
                lrPair1 = jChannel.LRpairGtreq1vecInChannelB.at( (uaTop % 2) == 0 ? i1 : j1);
                lrPair2 = jChannel.LRpairGtreq1vecInChannelB.at( i2 ); 

                lrBPairUaZero = makeGtreq1(lrMinusPair(lrPair1,lrPair2)); 

                /************
                 *  ua == 1  *
                 ************/
                lrPair1 = jChannel.LRpairGtreq1vecInChannelB.at( ((uaTop+1) % 2) == 0 ? i1 : j1 );
                lrPair2 = jChannel.LRpairGtreq1vecInChannelB.at( j2 ); 

                lrBPairUaOne = makeGtreq1(lrMinusPair(lrPair1,lrPair2)); 
                splitProbIntoJointChannel(probNewYa, lrAPairNewYa, lrBPairUaZero, lrBPairUaOne);
            }
            else // bPlus == true
            {
                numOfIJ = 2;
                lrBPairForYaUaZero.reserve(numOfIJ);
                lrBPairForYaUaOne.reserve(numOfIJ);
                WbNewYaUaZeroSumUb.reserve(numOfIJ);                                
                WbNewYaUaOneSumUb.reserve(numOfIJ);                                

                /************
                 *  ua == 0  *
                 ************/
                lrPair1 = jChannel.LRpairGtreq1vecInChannelB.at( (uaTop % 2) == 0 ? i1 : j1);
                lrPair2 = jChannel.LRpairGtreq1vecInChannelB.at( i2 ); 

                lrBPairForYaUaZero.push_back( makeGtreq1(lrPlusPair(lrPair1,lrPair2,0,fallbackLR_b)) ); 
                lrBPairForYaUaZero.push_back( makeGtreq1(lrPlusPair(lrPair1,lrPair2,1,fallbackLR_b)) ); 

                // Sum over Ub = Sum over positive and negative D-values, with ub = 0.
                WbNewYaUaZeroSumUb.push_back(      //             signb1        signb2      ua ubTop ub
                      jChannel.probPolarizedJointChannel_PP(ya1,i1,j1, 1 ,ya2,i2,j2, 1 , uaTop, 0, 0,  0)+
                      jChannel.probPolarizedJointChannel_PP(ya1,i1,j1,-1 ,ya2,i2,j2,-1 , uaTop, 0, 0,  0)+
                      jChannel.probPolarizedJointChannel_PP(ya1,i1,j1, 1 ,ya2,i2,j2,-1 , uaTop, 0, 1,  0)+
                      jChannel.probPolarizedJointChannel_PP(ya1,i1,j1,-1 ,ya2,i2,j2, 1 , uaTop, 0, 1,  0));

                WbNewYaUaZeroSumUb.push_back(      //             signb1       signb2       ua ubTop ub
                      jChannel.probPolarizedJointChannel_PP(ya1,i1,j1, 1 ,ya2,i2,j2, 1 , uaTop, 0, 1,  0)+
                      jChannel.probPolarizedJointChannel_PP(ya1,i1,j1,-1 ,ya2,i2,j2,-1 , uaTop, 0, 1,  0)+
                      jChannel.probPolarizedJointChannel_PP(ya1,i1,j1, 1 ,ya2,i2,j2,-1 , uaTop, 0, 0,  0)+
                      jChannel.probPolarizedJointChannel_PP(ya1,i1,j1,-1 ,ya2,i2,j2, 1 , uaTop, 0, 0,  0)); 

                /************
                 *  ua == 1  *
                 *************/

                lrPair1 = jChannel.LRpairGtreq1vecInChannelB.at( ((uaTop + 1) % 2) == 0 ? i1 : j1);
                lrPair2 = jChannel.LRpairGtreq1vecInChannelB.at( j2 ); 

                lrBPairForYaUaOne.push_back( makeGtreq1(lrPlusPair(lrPair1,lrPair2,0,fallbackLR_b)) ); 
                lrBPairForYaUaOne.push_back( makeGtreq1(lrPlusPair(lrPair1,lrPair2,1,fallbackLR_b)) ); 

                WbNewYaUaOneSumUb.push_back(      //             signb1       signb2        ua ubTop ub
                      jChannel.probPolarizedJointChannel_PP(ya1,i1,j1, 1 ,ya2,i2,j2, 1 , uaTop, 1, 0,  0)+ 
                      jChannel.probPolarizedJointChannel_PP(ya1,i1,j1,-1 ,ya2,i2,j2,-1 , uaTop, 1, 0,  0)+
                      jChannel.probPolarizedJointChannel_PP(ya1,i1,j1, 1 ,ya2,i2,j2,-1 , uaTop, 1, 1,  0)+
                      jChannel.probPolarizedJointChannel_PP(ya1,i1,j1,-1 ,ya2,i2,j2, 1 , uaTop, 1, 1,  0));

                WbNewYaUaOneSumUb.push_back(      //             signb1       signb2        ua ubTop ub
                      jChannel.probPolarizedJointChannel_PP(ya1,i1,j1, 1 ,ya2,i2,j2, 1 , uaTop, 1, 1,  0)+ 
                      jChannel.probPolarizedJointChannel_PP(ya1,i1,j1,-1 ,ya2,i2,j2,-1 , uaTop, 1, 1,  0)+
                      jChannel.probPolarizedJointChannel_PP(ya1,i1,j1, 1 ,ya2,i2,j2,-1 , uaTop, 1, 0,  0)+
                      jChannel.probPolarizedJointChannel_PP(ya1,i1,j1,-1 ,ya2,i2,j2, 1 , uaTop, 1, 0,  0));

                for( int i = 0; i < numOfIJ ; i++)
                {
                    for( int j = 0; j < numOfIJ ; j++)
                    {
                        lrBPairUaZero = lrBPairForYaUaZero.at(i);
                        lrBPairUaOne  = lrBPairForYaUaOne.at(j);

                        alpha_i = computeAlpha(i, WaNewYaGivenUaZero, WbNewYaUaZeroSumUb.at(i)); 
                        alpha_j = computeAlpha(j, WaNewYaGivenUaOne , WbNewYaUaOneSumUb.at(j) ); 
                        alphaij = alpha_i*alpha_j;
                        splitProbIntoJointChannel(alphaij * probNewYa, lrAPairNewYa, lrBPairUaZero, lrBPairUaOne);
                    }
                }
            }
        }
    }
}

myfloat JointChannel::probYaijUaLRGivenUb(int yaindex, int i, int j, int ua,
        int lrGtreq1Index, int signLLR, int ub ) const 
{
    myfloat lrFactor;

    assert (ua == 0 || ua == 1);
    assert (ub == 0 || ub == 1);
    assert (signLLR == 1 || signLLR == -1);

    int u; 
    u = ( (signLLR == 1) ? 0 : 1 ); 
    
    if ( ua == 0 )
    {
        if ( lrGtreq1Index != i )
        {
            return 0.0;
        }
        else
        {
            lrFactor = computeLRfactorFromPair(LRpairGtreq1vecInChannelB.at(i), u);
        }
    }
    else //  ua == 1 
    {
        if ( lrGtreq1Index != j )
        {
            return 0.0;
        }
        else
        {
            lrFactor = computeLRfactorFromPair(LRpairGtreq1vecInChannelB.at(j), u);
        }
    }

    if ( ub == 0 )
    {
      return 0.5*probYaijGivenUa(yaindex, i, j, ua)*lrFactor; 
    }
    else
    {
      return 0.5*probYaijGivenUa(yaindex, i, j, ua)*(1.0-lrFactor); 
    }
}

myfloat JointChannel::probPolarizedJointChannel_MM(int ya1, int i1, int j1, int signb1, int ya2,
        int i2, int j2, int signb2, int ua, int uaBot, int ub) const
{
    assert(ua     == 0 || ua     ==  1); 
    assert(uaBot  == 0 || uaBot  ==  1); 
    assert(ub     == 0 || ub     ==  1); 
    assert(signb1 == 1 || signb1 == -1); 
    assert(signb2 == 1 || signb2 == -1); 

    int ub1, ub2; 
    int ua1, ua2; 
    int ij1, ij2; 
    myfloat prob1, prob2;
    myfloat outputProb=0.0; 

    ua2 = uaBot; 
    ua1 = (ua + ua2)%2; 

    ij1 = ua1 == 0 ? i1 : j1;   
    ij2 = ua2 == 0 ? i2 : j2;   

    for (ub2 = 0; ub2<2 ; ub2++)
    {
        ub1 = (ub + ub2)%2;        

        prob1 = probYaijUaLRGivenUb(ya1, i1, j1, ua1, ij1, signb1, ub1); 
        prob2 = probYaijUaLRGivenUb(ya2, i2, j2, ua2, ij2, signb2, ub2); 
        outputProb += prob1*prob2;  
    }
    return outputProb/2.0; 
}



myfloat JointChannel::probPolarizedJointChannel_MP(int ya1, int i1, int j1, int signb1, int ya2,
        int i2, int j2, int signb2, int ua, int uaBot, int ubTop, int ub) const
{
    assert(ua     == 0 || ua     ==  1); 
    assert(uaBot  == 0 || uaBot  ==  1); 
    assert(ubTop  == 0 || ubTop  ==  1); 
    assert(ub     == 0 || ub     ==  1); 
    assert(signb1 == 1 || signb1 == -1); 
    assert(signb2 == 1 || signb2 == -1); 

    int ub1, ub2; 
    int ua1, ua2; 
    int ij1, ij2; 
    myfloat prob1, prob2;
    myfloat outputProb=0.0; 

    ua2 = uaBot; 
    ua1 = (ua + ua2)%2; 

    ij1 = ua1 == 0 ? i1 : j1;   
    ij2 = ua2 == 0 ? i2 : j2;   

    ub2 = ub; 
    ub1 = (ubTop + ub2)%2; 

    prob1 = probYaijUaLRGivenUb(ya1, i1, j1, ua1, ij1, signb1, ub1); 
    prob2 = probYaijUaLRGivenUb(ya2, i2, j2, ua2, ij2, signb2, ub2); 
    outputProb = prob1*prob2;  

    return outputProb/2.0; 
}


myfloat JointChannel::probPolarizedJointChannel_PM(int ya1, int i1, int j1, int signb1, int ya2,
        int i2, int j2, int signb2, int uaTop, int ua, int ub) const
{
    assert(uaTop  == 0 || uaTop  ==  1); 
    assert(ua     == 0 || ua     ==  1); 
    assert(ub     == 0 || ub     ==  1); 
    assert(signb1 == 1 || signb1 == -1); 
    assert(signb2 == 1 || signb2 == -1); 

    int ub1, ub2; 
    int ua1, ua2; 
    int ij1, ij2; 
    myfloat prob1, prob2;
    myfloat outputProb=0.0; 

    ua2 = ua; 
    ua1 = (uaTop + ua2)%2; 

    ij1 = ua1 == 0 ? i1 : j1;   
    ij2 = ua2 == 0 ? i2 : j2;   

    for (ub2 = 0; ub2<2 ; ub2++)
    {
        ub1 = (ub + ub2)%2;        

        prob1 = probYaijUaLRGivenUb(ya1, i1, j1, ua1, ij1, signb1, ub1); 
        prob2 = probYaijUaLRGivenUb(ya2, i2, j2, ua2, ij2, signb2, ub2); 
        outputProb += prob1*prob2;  
    }

    return outputProb/2.0; 
}

myfloat JointChannel::probPolarizedJointChannel_PP(int ya1, int i1, int j1, int signb1, int ya2,
        int i2, int j2, int signb2, int uaTop, int ua, int ubTop, int ub) const
{
    assert(uaTop  == 0 || uaTop  ==  1); 
    assert(ua     == 0 || ua     ==  1); 
    assert(ubTop  == 0 || ubTop  ==  1); 
    assert(ub     == 0 || ub     ==  1); 
    assert(signb1 == 1 || signb1 == -1); 
    assert(signb2 == 1 || signb2 == -1); 

    int ub1, ub2; 
    int ua1, ua2; 
    int ij1, ij2; 
    myfloat prob1, prob2;
    myfloat outputProb=0.0; 

    ua2 = ua; 
    ua1 = (uaTop + ua2)%2; 

    ij1 = ua1 == 0 ? i1 : j1;   
    ij2 = ua2 == 0 ? i2 : j2;   

    ub2 = ub; 
    ub1 = (ubTop + ub2)%2; 

    prob1 = probYaijUaLRGivenUb(ya1, i1, j1, ua1, ij1, signb1, ub1); 
    prob2 = probYaijUaLRGivenUb(ya2, i2, j2, ua2, ij2, signb2, ub2); 
    outputProb = prob1*prob2;  

    return outputProb/2.0; 
}

myfloat JointChannel::computeAlpha(myfloat i, myfloat WaNewYa, myfloat WbNewYaUaSumUb) const
{
    assert( WaNewYa >= 0.0 ); 
    assert( WbNewYaUaSumUb >= 0.0 ); 
    assert( WaNewYa - WbNewYaUaSumUb >= -smallestPositiveProb); 

    if (WaNewYa < verySmallProbability)
    {
        return ( (i == 0)? 1.0 : 0.0 ); 
    }
    else 
    {
        return WbNewYaUaSumUb / WaNewYa ; 
    }

}

myfloat JointChannel::signYa(int yaindex) const
{
    return ( (yaindex % 2 == 0) ? 1.0 : -1.0 ); 
}


myfloat JointChannel::computeLRfactorFromPair(LRpair lrPair, int u) const 
{
    assert(u==0 || u==1);
    assert(lrPair.first > 0.0 || lrPair.second > 0.0); 

    if(u == 0)
    {
        return lrPair.first/(lrPair.first + lrPair.second) ;
    }
    else // u == 1
    {
        return lrPair.second/(lrPair.first + lrPair.second); 
    }
}

LRpair JointChannel::lrMinusPair(LRpair lrPair1, LRpair lrPair2) const
{
    myfloat a1,a2,b1,b2; 
    a1 = lrPair1.first;
    b1 = lrPair1.second;

    a2 = lrPair2.first;
    b2 = lrPair2.second;
    
    return make_pair( (a1*a2 + b1*b2)/2.0 , (b1*a2 + a1*b2)/2.0 );
}

LRpair JointChannel::lrPlusPair(LRpair lrPair1, LRpair lrPair2, int u, LRpair fallbackLR) const
{
    assert(u == 0 || u == 1); 

    myfloat a1,a2,b1,b2; 
    myfloat p0, p1; 

    a1 = lrPair1.first;
    b1 = lrPair1.second;

    a2 = lrPair2.first;
    b2 = lrPair2.second;

    if (u == 0)
    {
        p0 = (a1*a2)/2.0; 
        p1 = (b1*b2)/2.0;
    }
    else // u == 1
    {
        p0 = (b1*a2)/2.0;
        p1 = (a1*b2)/2.0;
    }

    if ( p0 == 0.0 && p1 == 0.0)
    {
        return fallbackLR;
    }
    else
    {
        assert(p0 > 0.0 || p1 > 0.0);
        return make_pair(p0,p1); 
    }
}

LRpair JointChannel::makeGtreq1(LRpair lrPair) const
{
    if (lrPair.first >= lrPair.second)
    {
        return lrPair;
    }
    else
    {
        return make_pair(lrPair.second,lrPair.first);
    }
}

myfloat JointChannel::computeLRfromPair(LRpair lrPair) const        
{
    assert(lrPair.first >= 0.0 && lrPair.second >= 0.0); 
    if(lrPair.first == 0.0 && lrPair.second == 0.0)
    {
        return -1.0; // Just a marker for a rubbish value.
    }
    else
    {
        return lrPair.first/lrPair.second; 
    }

}


myfloat JointChannel::computeLRGtreq1fromPair(LRpair lrPair) const 
{
    assert(lrPair.first >= 0.0 && lrPair.second >= 0.0); 
    assert(lrPair.first > 0.0  ||  lrPair.second > 0.0); 

    if(lrPair.first >= lrPair.second)
    {
        return lrPair.first/lrPair.second; 
    }
    else
    {
        return lrPair.second/lrPair.first; 
    }
}

LRpair JointChannel::signedLRPair(LRpair lrPair, int signLLR) const
{
    assert(signLLR == 1 || signLLR == -1);
    if (signLLR == 1)
    {
        return lrPair; 
    }
    else // signLLR == -1
    {
        return make_pair(lrPair.second, lrPair.first); 
    }
}
