/* MergedLRsDataStructure.cpp   
 *
 * Copyright (C) 2010 Ido Tal <idotal@ieee.org> and Alexander Vardy <avardy@ucsd.edu>
 *
 * This file is part of polarError. If you wish to use it, please cite: 
 * B. Shuval, I. Tal ``A Lower Bound on The Probability of Error of Polar Codes over BMS Channels,''
 * arxiv:1701.01628.
 * 
 * and 
 *
 * I. Tal, A. Vardy ``How To Construct Polar Codes,''  IEEE Transactions on Information Theory, vol.
 * 59, no. 10, pp. 6562-6582, October 2013. 
 *
 * polarBounds is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or (at
 * your option) any later version.
 *
 * polarError is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with polarError; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */


#include "MergedLRsDataStructure.h"
#include "KahanSum.h"
#include <assert.h>
#include "ChannelMergeParameters.h"

using namespace std;

const MergedLRDatum *MergedLRsDataStructure::getHead() const
{
    const MergedLRDatum *mlrd;

    mlrd = heap[0];

    while (mlrd->prev != nullptr)
    {
        mlrd = mlrd->prev;
    }

    return mlrd;

}

MergedLRsDataStructure::MergedLRsDataStructure(const Channel &channel, const ChannelMergeParameters *ammparam)
{
    mmparam=ammparam;

    count = channel.getOutputAlphabetSize()/2; //!!

    buildArray(channel);
    buildHeap();
}

MergedLRsDataStructure::~MergedLRsDataStructure()
{
    delete[] array;
    delete[] heap;
}

int MergedLRsDataStructure::getCount() const
{
    return count;
}

void MergedLRsDataStructure::buildArray(const Channel &channel)
{
    int currentLR;

    array = new MergedLRDatum[count];

    // populate array with everything except the mergeWithNextCapacityDelta
    for ( currentLR = 0; currentLR < count; currentLR++)
    {
        // fill prev and next fields
        if ( currentLR == 0 )
        {
            array[currentLR].prev = nullptr;
        }
        else
        {
            array[currentLR].prev = &array[currentLR-1];
        }

        if ( currentLR ==  count - 1)
        {
            array[currentLR].next = nullptr;
        }
        else
        {
            array[currentLR].next = &array[currentLR+1];
        }

        // set originalLRsSet to just one element
        array[currentLR].firstLRIndex = array[currentLR].lastLRIndex = currentLR;

        // set peven and podd
        array[currentLR].peven = channel.getProbOutputGivenInput(2*currentLR,0); //!!
        array[currentLR].podd = channel.getProbOutputGivenInput(2*currentLR+1,0); //!!
    }

    // populate mergeWithNextCapacityDelta field in array
    for ( currentLR = 0; currentLR < count; currentLR++)
    {
        writeCapacityDifference(&array[currentLR]);
    }
}

void MergedLRsDataStructure::writeCapacityDifference(MergedLRDatum *mlrd)
{
    myfloat delta;

    delta = mmparam->getCapacityDifference(mlrd);

    assert(isnan(delta) == false);

    mlrd->mergeWithNextCapacityDelta = delta;
}

void MergedLRsDataStructure::buildHeap()
{
    int currentLR;

    heap = new MergedLRDatum*[count];

    for ( currentLR = 0; currentLR < count; currentLR++)
    {
        heap[currentLR] = &array[currentLR];
        array[currentLR].indexInHeap = currentLR;
    }

    for ( currentLR = count - 1; currentLR >= 0; currentLR--)
    {
        bubbleDown(currentLR);
    }
}

void MergedLRsDataStructure::heapSwitch(int son)
{
    MergedLRDatum *temp;
    int father;

    assert( son != 0 && son < count);

    getFather(son, father);

    temp = heap[father];
    heap[father] = heap[son];
    heap[father]->indexInHeap = father;

    heap[son] = temp;
    heap[son]->indexInHeap = son;
}

void MergedLRsDataStructure::getFather(int son, int &father)
{
    father = (son+1)/2 - 1;
}

void MergedLRsDataStructure::getSons(int father, int &leftSon, int &rightSon)
{
    leftSon = 2*(father+1)-1;
    rightSon = 2*(father+1)-1+1;
}

const myfloat& MergedLRsDataStructure::getHeapKey(int index)
{
    return heap[index]->mergeWithNextCapacityDelta;
}

bool MergedLRsDataStructure::bubbleDown(int father)
{
    int leftSon, rightSon;
    myfloat minKey;
    int minimizingSon;
    bool changedFlag = false;

    while (true)
    {
        getSons(father, leftSon, rightSon);

        minKey = getHeapKey(father);
        minimizingSon = -1;

        if ( leftSon < count && getHeapKey(leftSon) < minKey )
        {
            minKey = getHeapKey(leftSon);
            minimizingSon = leftSon;

        }

        if ( rightSon < count && getHeapKey(rightSon) < minKey )
        {
            minKey = getHeapKey(rightSon);
            minimizingSon = rightSon;

        }

        if (minimizingSon != -1 )
        {
            heapSwitch(minimizingSon);
            father = minimizingSon;
            changedFlag = true;
        }
        else
        {
            return changedFlag;
        }
    }
}

bool MergedLRsDataStructure::bubbleUp(int son)
{
    int father;
    bool changedFlag;
    changedFlag = false;

    while (son > 0)
    {
        getFather(son, father);

        if ( getHeapKey(son) < getHeapKey(father) )
        {
            heapSwitch(son);
            son = father;
            changedFlag = true;
        }
        else
        {
            return changedFlag;
        }

    }

    return changedFlag;
}

MergedLRDatum *MergedLRsDataStructure::pop()
{

    MergedLRDatum *popped;
    MergedLRDatum *temp;
    MergedLRDatum *next, *prev;

    temp = heap[count-1];
    heap[count-1] = heap[0];
    heap[0] = temp;

    heap[0]->indexInHeap = 0;

    count--;
    bubbleDown(0);


    popped = heap[count];
    next = popped->next;
    prev = popped->prev;

    if (next != nullptr)
    {
        next->prev = prev;
    }

    if (prev != nullptr)
    {
        prev->next = next;
    }

    return popped;
}

myfloat MergedLRsDataStructure::getTopKey() const
{
    assert(count > 0);

    MergedLRDatum *top;

    top = heap[0];

    return mmparam->getCapacityDifference(top);
}

void MergedLRsDataStructure::keyChanged(MergedLRDatum *mlrd)
{
    int heapIndex = mlrd->indexInHeap;
    bool wasMoved;

    wasMoved = bubbleDown(heapIndex);
    if (wasMoved == false)
    {
        bubbleUp(heapIndex);
    }

}

void MergedLRsDataStructure::createNewProbsArray(myfloat *&newProbsArray, int &newOutputAlphabetCount, const Channel *channel)
{
    newOutputAlphabetCount = 2*count;
    newProbsArray = new myfloat[newOutputAlphabetCount];

    mmparam->createNewProbsArray(newProbsArray, channel, *this);
}

Channel MergedLRsDataStructure::createNewChannel(const Channel &oldChannel)
{
    int newOutputAlphabetCount;
    int i;
    myfloat *newProbsArray;

    createNewProbsArray(newProbsArray, newOutputAlphabetCount, &oldChannel);

    Channel newChannel(newOutputAlphabetCount);

    for( i = 0; i < newOutputAlphabetCount; i+=2 )
    {
        newChannel.addProbOfConjugatePairGivenInputZero(newProbsArray[i], newProbsArray[i+1]);
    }

    delete[] newProbsArray;

    return newChannel;
}

void MergedLRsDataStructure::sanityCheck() const
{
    MergedLRDatum *mlrd;
    bool sane = true;
    int i;

    for ( i = 0; i < count; i++ )
    {
        mlrd = heap[i];

        if (mlrd->indexInHeap != i )
        {
            sane = false;
        }
    }

    if ( sane == false )
    {
        printSelf(cout);
        assert(false);
    }
}

void MergedLRsDataStructure::printSelf(std::ostream& output) const
{
    int i;
    MergedLRDatum *mlrd;

    output << "The data structure contains " << count << " elements." << endl;

    for ( i = 0; i < count; i++ )
    {
        mlrd = heap[i];

        output << "* Element " << i << " has address " << mlrd << endl;

        output << "prev = " << mlrd->prev << endl;
        output << "next = " << mlrd->next << endl;
        output << "firstLRIndex = " << mlrd->firstLRIndex << endl;
        output << "lastLRIndex = " << mlrd->lastLRIndex << endl;
        output << "peven = " << mlrd->peven << endl;
        output << "podd = " << mlrd->podd << endl;
        output << "mergeWithNextCapacityDelta = " << mlrd->mergeWithNextCapacityDelta << endl;
        output << "indexInHeap = " << mlrd->indexInHeap << endl << endl;
    }
}

void MergedLRsDataStructure::mergeUntilGood(int verbosity)
{
    if ( verbosity > 0 )
    {
        printSelf(cout);
    }

    // merge until OK to stop
    while (mmparam->stoppingCondition(*this) == false)
    {
        MergedLRDatum *currentDatum, *nextDatum, *prevDatum;

        currentDatum = pop();


        // merge currentDatum into its neighbors

        mmparam->updateProbsAfterMerge(currentDatum);
        mmparam->updateLRIndices(currentDatum);

        nextDatum = currentDatum->next;
        prevDatum = currentDatum->prev;

        if (mmparam->nextDatumKeyChanges() == true && nextDatum != nullptr)
        {
            writeCapacityDifference(nextDatum);
            keyChanged(nextDatum);
        }

        if (mmparam->prevDatumKeyChanges() == true && prevDatum != nullptr)
        {
            writeCapacityDifference(prevDatum);
            keyChanged(prevDatum);
        }

        if (verbosity > 1)
        {
            printSelf(cout);
        }
    }
}
