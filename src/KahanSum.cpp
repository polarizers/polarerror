/* KahanSum.cpp
 *
 * Copyright (C) 2010 Ido Tal <idotal@ieee.org> and Alexander Vardy <avardy@ucsd.edu>
 *
 * This file is part of polarError. If you wish to use it, please cite: 
 * B. Shuval, I. Tal ``A Lower Bound on The Probability of Error of Polar Codes over BMS Channels,''
 * arxiv:1701.01628.
 * 
 * and 
 *
 * I. Tal, A. Vardy ``How To Construct Polar Codes,''  IEEE Transactions on Information Theory, vol.
 * 59, no. 10, pp. 6562-6582, October 2013. 
 *
 * polarError is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or (at
 * your option) any later version.
 *
 * polarError is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with polarError; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */

#include "KahanSum.h"
#include <assert.h>

KahanSum::KahanSum()
{
    startNewSum();
}

KahanSum::~KahanSum()
{
}

void KahanSum::insert(const myfloat &f)
{
    //TODO: if ultimately decide to round up/down, then change this so that is is also true for the sum (not so right now)
    y = f - c;
    t = sum + y;
    c = (t - sum) - y;
    sum = t;
}

void KahanSum::startNewSum()
{
    c = 0.0;
    sum = 0.0;
}

myfloat KahanSum::calcSum()
{
    return sum;
}


