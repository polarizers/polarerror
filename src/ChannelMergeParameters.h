/* ChannelMergeParameters.h   
 *
 * Copyright (C) 2010 Ido Tal <idotal@ieee.org> and Alexander Vardy <avardy@ucsd.edu>
 *
 * This file is part of polarError. If you wish to use it, please cite: 
 * B. Shuval, I. Tal ``A Lower Bound on The Probability of Error of Polar Codes over BMS Channels,''
 * arxiv:1701.01628.
 * 
 * and 
 *
 * I. Tal, A. Vardy ``How To Construct Polar Codes,''  IEEE Transactions on Information Theory, vol.
 * 59, no. 10, pp. 6562-6582, October 2013. 
 *
 * polarBounds is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or (at
 * your option) any later version.
 *
 * polarError is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with polarError; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */

#ifndef CHANNEL_MERGE_PARAMETERS
#define CHANNEL_MERGE_PARAMETERS

#include "myfloat.h"
#include "KahanSum.h"

class MergedLRsDataStructure;
struct MergedLRDatum;
class Channel;

// --- Virtual classes ----------------------------------------------------------------------------
/// The base class used to communicate all the merging interfaces. Meant to be inherited as public virtual.
class ChannelMergeParameters
{
public:
    virtual ~ChannelMergeParameters();

    /// Get the difference in capacity when removing mlrd (merging it into either mlrd->next, or into both mlrd->next and mlrd->prev).
    virtual myfloat getCapacityDifference(const MergedLRDatum *mlrd) const = 0;

    /// We've decided to remove mlrd. Update the probabilities in either mlrd->next, or in both mlrd->next and mlrd->prev.
    virtual void updateProbsAfterMerge(const MergedLRDatum *mlrd) const = 0;

    virtual bool stoppingCondition(const MergedLRsDataStructure &ds) const = 0;

    /// Create the new probsArray for channel. Generally, newProbsArray is *not* canonicalized.
    virtual void createNewProbsArray(myfloat *newProbsArray, const Channel *channel, const MergedLRsDataStructure &ds) const = 0;

    virtual void updateLRIndices(const MergedLRDatum *mlrd) const = 0;
    virtual bool nextDatumKeyChanges() const = 0;
    virtual bool prevDatumKeyChanges() const = 0;

    static bool LRisInfinity(const myfloat &yodd, const myfloat &yeven);

    /// calculate log((1+d1)/(1+d2)), where d1,d2 > -1
    static myfloat mylogFrac(const myfloat &d1, const myfloat &d2);

    /// calculate log(1+d), where d > -1
    static myfloat mylog(const myfloat &d);
protected:
    void calcSumProbs(myfloat &sumyeven, myfloat &sumyodd, int firstLRIndex, int lastLRIndex, const Channel *channel) const;
    void getProbs(myfloat &yeven, myfloat &yodd, int LRIndex, const Channel *channel) const;
    mutable KahanSum ksEven;
    mutable KahanSum ksOdd;
};

// --- Interfaces ---------------------------------------------------------------------------------

class ChannelMergeParameters_stopWhenAlphabetSmallEnough : virtual public ChannelMergeParameters
{
public:
    ChannelMergeParameters_stopWhenAlphabetSmallEnough(int aM);
    virtual bool stoppingCondition(const MergedLRsDataStructure &ds) const;
private:
    int M;

};

class PairMerge : virtual public ChannelMergeParameters
{
public:
    virtual ~PairMerge() {};

    //myfloat getPairCapacityDifference(const MergedLRDatum *current) const;
    virtual myfloat getCapacityDifference(const MergedLRDatum *current) const;
    virtual void updateProbsAfterMerge(const MergedLRDatum *current) const;
    virtual void createNewProbsArray(myfloat *newProbsArray, const Channel *channel, const MergedLRsDataStructure &ds) const;
    virtual void updateLRIndices(const MergedLRDatum *mlrd) const;
    virtual bool nextDatumKeyChanges() const;
    virtual bool prevDatumKeyChanges() const;

protected:
    virtual myfloat getPairCapacityDifference(const myfloat &y1,const myfloat &y1bar, const myfloat &y2, const myfloat &y2bar) const = 0;
    virtual void updatePairProbsAfterMerge(myfloat &nextDatum_peven, myfloat &nextDatum_podd, const myfloat &currentDatum_peven, const myfloat &currentDatum_podd) const = 0;
    virtual void createNewProbsArray_Pair(myfloat &newyeven, myfloat &newyodd, int firstLRIndex, int lastLRIndex, const Channel *channel) const = 0;

};

// --- Instantiable classes -----------------------------------------------------------------------
class ChannelMergeParameters_twoWayDegrade : public ChannelMergeParameters_stopWhenAlphabetSmallEnough, public PairMerge
{
public:
    ChannelMergeParameters_twoWayDegrade(int aM);

    /// Get the difference in capacity when the symbol pair corresponging to mlrd is renamed (degraded) to the symbol pair corresponding to mlrd->next.
    //virtual myfloat getCapacityDifference(const MergedLRDatum *mlrd) const;
protected:
    virtual myfloat getPairCapacityDifference(const myfloat &y1,const myfloat &y1bar, const myfloat &y2, const myfloat &y2bar) const;
    virtual void updatePairProbsAfterMerge(myfloat &nextDatum_peven, myfloat &nextDatum_podd, const myfloat &currentDatum_peven, const myfloat &currentDatum_podd) const;
    virtual void createNewProbsArray_Pair(myfloat &newyeven, myfloat &newyodd, int firstLRIndex, int lastLRIndex, const Channel *channel) const;
};

class ChannelMergeParameters_twoWayUpgrade : public ChannelMergeParameters_stopWhenAlphabetSmallEnough
{
public:
    ChannelMergeParameters_twoWayUpgrade(int aM);

    /// Let LR_next and LR_prev be the LRs corresponding to mlrd->next and mlrd->prev.
    /// Merge (upgrade) mlrd into two symbol pairs with LRs LR_next and LR_prev, so that the reverse degrading operation gives us the current state.
    virtual myfloat getCapacityDifference(const MergedLRDatum *mlrd) const;
    virtual void updateProbsAfterMerge(const MergedLRDatum *mlrd) const;
    virtual void createNewProbsArray(myfloat *newProbsArray, const Channel *channel, const MergedLRsDataStructure &ds) const;
    virtual void updateLRIndices(const MergedLRDatum *mlrd) const;
    virtual bool nextDatumKeyChanges() const;
    virtual bool prevDatumKeyChanges() const;

private:
    void calcPrivates(const MergedLRDatum *mlrd) const;
    void calcPrivates(const myfloat &ax, const myfloat &ay, const myfloat &prevEven, const myfloat &prevOdd, const myfloat &nextEven, const myfloat &nextOdd) const;
    mutable myfloat alpha, beta, lambda1;
    mutable myfloat x, y, lambda2;
    mutable myfloat gamma, delta, lambda3;
    mutable KahanSum ksNextEven;
    mutable KahanSum ksNextOdd;

};

class ChannelMergeParameters_oneWayUpgrade : virtual public ChannelMergeParameters, public PairMerge
{
public:
    ChannelMergeParameters_oneWayUpgrade(myfloat aEpsilon);

    /// Here, we are not returning the "capacity difference", but rather how close the LRs are.
    //virtual myfloat getCapacityDifference(const MergedLRDatum *mlrd) const;
    virtual bool stoppingCondition(const MergedLRsDataStructure &ds) const;
protected:
    virtual myfloat getPairCapacityDifference(const myfloat &y1,const myfloat &y1bar, const myfloat &y2, const myfloat &y2bar) const;
    virtual void updatePairProbsAfterMerge(myfloat &nextDatum_peven, myfloat &nextDatum_podd, const myfloat &currentDatum_peven, const myfloat &currentDatum_podd) const;
    virtual void createNewProbsArray_Pair(myfloat &newyeven, myfloat &newyodd, int firstLRIndex, int lastLRIndex, const Channel *channel) const;
private:
    myfloat epsilon;
};

#endif //CHANNEL_MERGE_PARAMETERS

