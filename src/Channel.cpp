 /* Channel.cpp
 *
 * Copyright (C) 2018 Boaz Shuval <bshuval@campus.technion.ac.il> and Ido Tal <idotal@ieee.org> 
 *
 * This file is part of polarError. If you wish to use it, please cite: 
 * B. Shuval, I. Tal ``A Lower Bound on The Probability of Error of Polar Codes over BMS Channels,''
 * arxiv:1701.01628.
 * 
 * and 
 *
 * I. Tal, A. Vardy ``How To Construct Polar Codes,''  IEEE Transactions on Information Theory, vol.
 * 59, no. 10, pp. 6562-6582, October 2013. 
 *
 * polarError is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or (at
 * your option) any later version.
 *
 * polarError is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with polarError; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */  
#include "Channel.h"
#include "ChannelMergeParameters.h"
#include "MergedLRsDataStructure.h"

#include <assert.h>
#include <algorithm>
#include <iostream>
#include <numeric>


using namespace std;

const myfloat dValueSortEpsilon = 0.0;
const myfloat  uniqueLREpsilon = 0.0001;

bool dValuesEpsilonUnique( myfloat a, myfloat b)
{
    if ( abs(a-b) < dValueSortEpsilon )
    {
        return true;
    }
    else
    {
        return false;
    }
}

Channel::Channel()
{
} 

Channel::Channel(int projectedOutputAlphabetSize )
{
    assert ( projectedOutputAlphabetSize > 0 );
    assert ( projectedOutputAlphabetSize % 2 == 0 );

    probGivenZero.reserve(projectedOutputAlphabetSize);
}

Channel::Channel( const Channel &other )
{
    assert(false);

    probGivenZero = other.probGivenZero;
}

Channel::~Channel()
{

}

Channel &Channel::operator=(const Channel &other )
{
    probGivenZero = other.probGivenZero;

    return *this;
}

int Channel::getOutputAlphabetSize() const
{
    return probGivenZero.size();
}

void Channel::addProbOfConjugatePairGivenInputZero(myfloat evenProb, myfloat oddProb)
{

    probGivenZero.push_back(evenProb);
    probGivenZero.push_back(oddProb);
}

myfloat Channel::getProbOutputGivenInput(int outputIndex, int input) const
{
    assert(outputIndex < getOutputAlphabetSize() && outputIndex >= 0); 
    assert(input == 0 || input == 1); 

    if(input==0)
    {
        return probGivenZero[outputIndex]; 
    }
    else // input == 1
    {
        if(outputIndex%2 == 0)
        {
            return probGivenZero[outputIndex + 1]; 
        }
        else
        {
            return probGivenZero[outputIndex - 1]; 
        }
    }
}


vector<myfloat> Channel::getAbsDValues(bool assumeCanonicalized) const
{
    vector<myfloat> dValues;
    myfloat sum, diff, dValue;

    dValues.reserve(probGivenZero.size());

    vector<myfloat>::const_iterator i; 

    for (i = probGivenZero.begin(); i != probGivenZero.end(); i+=2)
    {
        sum = *i + *(i+1);
        diff = *i - *(i+1);

        assert( *i >= 0.0 && *(i+1) >= 0.0 );

        if ( *i == 0.0 && *(i+1) == 0.0 )
        {
            if ( assumeCanonicalized == true )
            {
                assert(false);
            }
        }
        else
        {

            dValue = abs(diff/sum);
            dValues.push_back(dValue);

        }

    }


    if ( assumeCanonicalized == true )
    {
        assert(unique(dValues.begin(), dValues.end(), dValuesEpsilonUnique) == dValues.end());
    }
    else
    {
        sort(dValues.begin(), dValues.end());
        reverse(dValues.begin(), dValues.end());
        dValues.erase(unique(dValues.begin(), dValues.end(), dValuesEpsilonUnique), dValues.end());
    }

    return dValues;

}


vector<myfloat> Channel::getLRgtreq1(bool assumeCanonicalized) const
{
    vector<myfloat> LRvec;
    myfloat p0, p1, lr;

    LRvec.reserve(probGivenZero.size());

    vector<myfloat>::const_iterator i; 

    for (i = probGivenZero.begin(); i != probGivenZero.end(); i+=2)
    {
        p0 = *i;
        p1 = *(i+1);

        assert( *i >= 0.0 && *(i+1) >= 0.0 );
        assert( p0 >= p1 );

        if ( p0 == 0.0 && p1 == 0.0 )
        {
            if ( assumeCanonicalized == true )
            {
                assert(false);
            }
        }
        else
        {
            lr = p0/p1;
            LRvec.push_back(lr);
        }
    }


    if (assumeCanonicalized == false)
    {
        sort(LRvec.begin(), LRvec.end());
        reverse(LRvec.begin(), LRvec.end());

        vector<myfloat> uniqueLRvec;
        vector<myfloat>::iterator i;
        i = uniqueLRvec.begin(); 
        uniqueLRvec.push_back(*i); 
        while( i+1 < uniqueLRvec.end() )
        {
            if( ( *(i+1) < plusInfinity ) &&  (*(i+1) / *(i) >= uniqueLREpsilon ) )
            {
                uniqueLRvec.push_back( *i );
            }
            i++; 
        }
        swap(LRvec, uniqueLRvec); 
    }
    return LRvec;
}

vector<pair<myfloat,myfloat>> Channel::getLRpairvec(bool assumeCanonicalized) const
{
    assert(assumeCanonicalized == true);

    vector<pair<myfloat,myfloat>> LRpairvec;
    myfloat p0, p1;

    LRpairvec.reserve(probGivenZero.size());

    vector<myfloat>::const_iterator i; 

    for (i = probGivenZero.begin(); i != probGivenZero.end(); i+=2)
    {
        p0 = *i;
        p1 = *(i+1);

        assert( *i >= 0.0 && *(i+1) >= 0.0 );
        assert( p0 >= p1 );

        if ( p0 == 0.0 && p1 == 0.0 )
        {
            assert(false);
        }
        else
        {
            LRpairvec.push_back( make_pair(p0,p1) );
        }
    }

    return LRpairvec;
}



void Channel::canonicalizeSelf()
{

    vector<myfloat>::iterator yeven, yodd; 
    myfloat evenProb, oddProb;
    int evenIndex;
    vector<pair<myfloat,int>> dValuesAndIndices;
    myfloat dValue;
    int uniqueDValuesCount;
    myfloat prevDValue;

    dValuesAndIndices.reserve( getOutputAlphabetSize()/2 );

    // Normalize the probabilities to 1
    normalizeSelf();

    // Make even probabilities greater than odd probabilities
    for (evenIndex = 0,yeven = probGivenZero.begin(), yodd=yeven+1; yeven !=
            probGivenZero.end(); yeven+=2,yodd+=2,evenIndex+=2)
    {
        evenProb = (*yeven);  
        oddProb = (*yodd);  

        if ( oddProb > evenProb )
        {
            *yeven  = oddProb;
            *yodd = evenProb;
        }

        if ( *yeven <= 0.0 )
        {
            // set to some invalid value
            dValue = -1.0;
        }
        else
        {
            dValue = (*yeven - *yodd ) / (*yeven + *yodd);
        }

        dValuesAndIndices.push_back(make_pair(dValue,evenIndex));
    }

    sort( dValuesAndIndices.begin(), dValuesAndIndices.end() );

    uniqueDValuesCount = 0;

    // count the number of unique D values

    // start by "seeing" the value -1.0, used above to denote
    // a zero-probability letter. We do not want to count such an element as
    // a unique D value
    prevDValue = -1.0;

    for( pair<myfloat,int> currentPair : dValuesAndIndices )
    {
        if ( currentPair.first != prevDValue  )
        {
            uniqueDValuesCount++;
            prevDValue = currentPair.first;
        }

    }

    // set -1.0 as the default prob, in order to catch errors later on
    vector<myfloat> newProbGivenZero(2*uniqueDValuesCount, -1.0);
    yeven = newProbGivenZero.end();
    yodd = yeven + 1;

    prevDValue = -1.0;

    for( pair<myfloat,int> currentPair : dValuesAndIndices )
    {
        if ( currentPair.first == -1.0 )
        {
            continue;
        }

        if ( currentPair.first != prevDValue  )
        {
            yeven -= 2;
            yodd -= 2;

            *yeven = probGivenZero[currentPair.second];
            *yodd = probGivenZero[currentPair.second+1];

            prevDValue = currentPair.first;
        }
        else
        {
            *yeven += probGivenZero[currentPair.second];
            *yodd += probGivenZero[currentPair.second+1];
        }
    }

    swap(newProbGivenZero, probGivenZero);
}

void Channel::normalizeSelf()
{
    myfloat sum;

    sum = 0.0;

    sum =  accumulate( probGivenZero.begin(), probGivenZero.end(), sum);

    assert (sum > 0.0 );

    for ( myfloat &f : probGivenZero )
    {
        f/=sum;
    }
}

void Channel::printSelf()
{
    printProbVector();
    printStats();
    printDValues();
    printLRs();


}
void Channel::printProbVector()
{

    cout << "Prob vector given 0 = ";
    for ( myfloat f : probGivenZero )
    {
        cout << f << " ";
    }
    cout << endl;
}

void Channel::printStats()
{
    cout << "The output alphabet size is " << getOutputAlphabetSize() << endl;
    cout << "The error probability is " << calcErrorProbability() << endl;
    cout << "The Bhattacharyya is " << calcBhattacharyya() << endl;
    cout << "The TV is " << calcTV() << endl;
    cout << "The symmetric capacity is " << 1.0 - calcSymmetricEntropy() << endl;
}

void Channel::printDValues()
{

    vector<myfloat> absDValues;

    cout << "The d values are" << endl;
    absDValues = getAbsDValues();

    for (myfloat f : absDValues )
    {
        cout << f << " ";
    }
    cout << endl;
}


void Channel::printLRs()
{

    vector<myfloat> LRvec;

    cout << "The LRs are" << endl;
    LRvec = getLRgtreq1();

    for (myfloat f : LRvec )
    {
        cout << f << " ";
    }
    cout << endl;
}

Channel Channel::minusTransform()
{

    int newOutputAlphabetSize = getOutputAlphabetSize()*getOutputAlphabetSize()/2;
    Channel newChannel(newOutputAlphabetSize);

    vector<myfloat>::const_iterator ya, yb; 
    myfloat evenProb, oddProb;

    for (ya = probGivenZero.begin(); ya != probGivenZero.end(); ya+=2)
    {
        for (yb = probGivenZero.begin(); yb != probGivenZero.end(); yb+=2)
        {
            evenProb = (*ya) * (*yb) + (*(ya+1)) * (*(yb+1));  
            oddProb = (*ya) * (*(yb+1)) + (*(ya+1)) * (*yb);  
            newChannel.addProbOfConjugatePairGivenInputZero(evenProb, oddProb);
        }
    }

    newChannel.canonicalizeSelf();
    return newChannel;
}

Channel Channel::plusTransform()
{
    int newOutputAlphabetSize = getOutputAlphabetSize()*getOutputAlphabetSize();
    Channel newChannel(newOutputAlphabetSize);

    vector<myfloat>::const_iterator ya, yb; 
    myfloat evenProb, oddProb;

    for (ya = probGivenZero.begin(); ya != probGivenZero.end(); ya+=2)
    {
        for (yb = probGivenZero.begin(); yb != probGivenZero.end(); yb+=2)
        {
            evenProb = (*ya) * (*yb);  
            oddProb = (*(ya+1)) * (*(yb+1));  
            newChannel.addProbOfConjugatePairGivenInputZero(evenProb, oddProb);

            evenProb = (*(ya+1)) * (*yb);  
            oddProb = (*ya) * (*(yb+1));  
            newChannel.addProbOfConjugatePairGivenInputZero(evenProb, oddProb);
        }
    }

    newChannel.canonicalizeSelf();
    return newChannel;
}

myfloat Channel::calcErrorProbability() const
{
    myfloat sumProb, sumMinProb;

    sumProb = 0.0;
    sumMinProb = 0.0;

    vector<myfloat>::const_iterator yeven, yodd; 

    // Make even probabilities greater than odd probabilities
    for (yeven = probGivenZero.begin(), yodd=yeven+1; yeven !=
            probGivenZero.end(); yeven+=2,yodd+=2)
    {
        sumProb += (*yeven) + (*yodd);  
        sumMinProb += min( (*yeven), (*yodd) );  

    }

    assert( sumProb > 0.0 );

    return sumMinProb/sumProb;
}

myfloat Channel::calcBhattacharyya() const
{
    myfloat sumProb, sumZ;

    sumProb = 0.0;
    sumZ = 0.0;

    vector<myfloat>::const_iterator yeven, yodd; 

    // Make even probabilities greater than odd probabilities
    for (yeven = probGivenZero.begin(), yodd=yeven+1; yeven !=
            probGivenZero.end(); yeven+=2,yodd+=2)
    {
        sumProb += (*yeven) + (*yodd);  
        sumZ += sqrt( (*yeven) * (*yodd) );  

    }

    assert( sumProb > 0.0 );

    // We've only summed half of the output alphabet letters, in effect
    return 2*sumZ/sumProb;
}

myfloat Channel::calcTV() const
{
    myfloat sumProb, sumTV;

    sumProb = 0.0;
    sumTV = 0.0;

    vector<myfloat>::const_iterator yeven, yodd; 

    // Make even probabilities greater than odd probabilities
    for (yeven = probGivenZero.begin(), yodd=yeven+1; yeven !=
            probGivenZero.end(); yeven+=2,yodd+=2)
    {
        sumProb += (*yeven) + (*yodd);  
        sumTV += abs( (*yeven) - (*yodd) );  

    }

    assert( sumProb > 0.0 );

    return sumTV/sumProb;
}

myfloat Channel::calcSymmetricEntropy() const
{
    myfloat sumProb, sumSymmetricEntropy;
    myfloat sumOfPair;

    sumProb = 0.0;
    sumSymmetricEntropy = 0.0;

    vector<myfloat>::const_iterator yeven, yodd; 

    // Make even probabilities greater than odd probabilities
    for (yeven = probGivenZero.begin(), yodd=yeven+1; yeven !=
            probGivenZero.end(); yeven+=2,yodd+=2)
    {
        sumOfPair = (*yeven) + (*yodd);
        sumProb += sumOfPair;  
        sumSymmetricEntropy += xlogy( (*yeven), (*yeven)/sumOfPair);  
        sumSymmetricEntropy += xlogy( (*yodd), (*yodd)/sumOfPair);  

    }

    assert( sumProb > 0.0 );

    return -sumSymmetricEntropy/sumProb;
}

void Channel::upgrade(Channel &newChannel, int M)
{
    assert(M % 2 == 0);

    // copy as is, if output alphabet size is already small enough
    if ( M >= getOutputAlphabetSize() )
    {
        newChannel = *this;
    }
    else
    {   
        Channel oneWayUpgradeChannel(getOutputAlphabetSize());
        // clean up "too-close" d-values
        ChannelMergeParameters_oneWayUpgrade cmp_oneWay(uniqueLREpsilon);

        MergedLRsDataStructure mlrds_oneWay(*this, &cmp_oneWay);
        mlrds_oneWay.mergeUntilGood();
        oneWayUpgradeChannel = mlrds_oneWay.createNewChannel(*this);
        
        // The real upgrade
        ChannelMergeParameters_twoWayUpgrade cmp_twoWay(M);

        MergedLRsDataStructure mlrds_twoWay(oneWayUpgradeChannel, &cmp_twoWay);
        mlrds_twoWay.mergeUntilGood();
        newChannel = mlrds_twoWay.createNewChannel(oneWayUpgradeChannel);
    }


}                                

myfloat Channel::xlogy(const myfloat &x, const myfloat &y) const
{
    if (x <= 0.0 || y <= 0.0)
    {
        return 0.0;
    }
    else
    {
        myfloat res;

        res = x*log(y);

        assert( isnan(res) == false );

        return res;
    }
}

myfloat Channel::getAbsDValue( int outputIndexEven ) const
{
    myfloat absDValue;
    

    assert ( outputIndexEven >= 0 && outputIndexEven < probGivenZero.size() &&
            outputIndexEven % 2 == 0 );

    
    absDValue = (getProbOutputGivenInput(outputIndexEven, 0)
            -  getProbOutputGivenInput(outputIndexEven, 1))/getPairProb(outputIndexEven);
    
    assert( absDValue >= 0.0 );

    return absDValue;
    

}

myfloat Channel::getPairProb( int outputIndexEven ) const
{
    myfloat pairProb;

    assert ( outputIndexEven >= 0 && outputIndexEven < probGivenZero.size() &&
            outputIndexEven % 2 == 0 );

    pairProb = (getProbOutputGivenInput(outputIndexEven, 0) +  getProbOutputGivenInput(outputIndexEven, 1));
    assert( pairProb >= 0.0 );

    return pairProb;
}

void Channel::printToFile(ofstream &filename)
{
    assert(filename.is_open() == true);    
    filename << "The output alphabet size is " << getOutputAlphabetSize() << endl;
    filename << "The error probability is    " << calcErrorProbability() << endl;
    filename << "The Bhattacharyya is        " << calcBhattacharyya() << endl;
    filename << "The TV is                   " << calcTV() << endl;
    filename << "The symmetric capacity is   " << 1.0 - calcSymmetricEntropy() << endl;

    filename << "Prob vector given 0 is " << endl; 
    for ( myfloat f : probGivenZero )
    {
        filename << f << " ";
    }
    filename << endl;
    
    vector<myfloat> absDValues;

    filename << "The d values are" << endl;
    absDValues = getAbsDValues();

    for (myfloat f : absDValues )
    {
        filename << f << " ";
    }
    filename << endl;
}
