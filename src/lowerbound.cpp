 /* LowerBound.cpp
 *
 * Copyright (C) 2018 Boaz Shuval <bshuval@campus.technion.ac.il> and Ido Tal <idotal@ieee.org> 
 *
 * This file is part of polarError. If you wish to use it, please cite: 
 * B. Shuval, I. Tal ``A Lower Bound on The Probability of Error of Polar Codes over BMS Channels,''
 * arxiv:1701.01628.
 * 
 * and 
 *
 * I. Tal, A. Vardy ``How To Construct Polar Codes,''  IEEE Transactions on Information Theory, vol.
 * 59, no. 10, pp. 6562-6582, October 2013. 
 *
 * polarError is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or (at
 * your option) any later version.
 *
 * polarError is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with polarError; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */  
#include "LowerBound.h"
#include "assert.h"
#include <algorithm>
#include <iostream>
#include <iomanip>


LowerBound::LowerBound(Channel &ainitialChannel, int aaChannelAlphabetSize, int abChannelAlphabetSize)
{
    assert(aaChannelAlphabetSize > 2 && ( aaChannelAlphabetSize % 2 == 0) );
    assert(abChannelAlphabetSize > 2 && ( abChannelAlphabetSize % 2 == 0) );

    initialChannel = ainitialChannel; 
    
    aChannelAlphabetSize = aaChannelAlphabetSize;
    bChannelAlphabetSize = abChannelAlphabetSize;

    areTransformListsLoaded = false; 
    
    lastSingleChannelIndex = 0; 
    lastSingleChannelLogLength = 0; 
    lastSingleChannel = ainitialChannel; 


}

LowerBound::~LowerBound()
{
}


void LowerBound::generateTransformList(int aChannelIndex, int bChannelIndex, int aLogLength)
{
    assert( aChannelIndex < bChannelIndex ); 
    int i;

    logLength = aLogLength; 
    aChannelTransformList = TransformList(logLength, aChannelIndex);
    bChannelTransformList = TransformList(logLength, bChannelIndex);

    for ( i = 0 ; i < logLength ; i++)
    {
        if ( aChannelTransformList.getTransform(i) != bChannelTransformList.getTransform(i) )
        {
            logIndexOfFirstJointChannel = i;
            break;
        }
    }

    areTransformListsLoaded = true; 

    // We should never reach this assert. 
    assert( i < aChannelTransformList.getLogLength() );
}

void LowerBound::printSelf(int whatToPrint) 
{
    // whatToPrint options: 
    // 0 = after initial constructor. 
    // 1 = after generating transform list.
    // 2 = after computing the last single channel.
    // 3 = results of the latest joint channel error computation.

    if( whatToPrint >= 0)
    {
        cout << "The initial channel is: " << endl;
        cout << "************************************************************" << endl; 
        initialChannel.printSelf(); 
        cout << "************************************************************" << endl; 
    }
    if( whatToPrint == 0)
    {
        cout << "The a-channel Alphabet Size is: " << aChannelAlphabetSize << endl;
        cout << "The b-channel Alphabet Size is: " << bChannelAlphabetSize << endl; 
        cout << "************************************************************" << endl; 
    }
    if( whatToPrint >= 1)
    {
        cout << "The a-channel Alphabet Size is: " << aChannelAlphabetSize;
        cout << ". Its transform list is: " << endl;
        aChannelTransformList.printSelf(); 
        cout << "The b-channel Alphabet Size is: " << bChannelAlphabetSize;
        cout << ". Its transform list is: " << endl; 
        bChannelTransformList.printSelf(); 
        cout << "The two transform lists differ on index " << logIndexOfFirstJointChannel << endl;
        cout << "************************************************************" << endl; 
    }

    if (whatToPrint >= 2)
    {
        cout << "The last single channel is: " << endl;
        cout << "************************************************************" << endl; 
        lastSingleChannel.printSelf(); 
        cout << "************************************************************" << endl; 
    }

    if (whatToPrint >= 3)
    {
        cout << "The latest Joint Channel Checked is: (aIndex, bIndex) = (";
        cout << imjpResultsVector.back().aChannelIndex << ", ";
        cout << imjpResultsVector.back().bChannelIndex <<  ") , at log-length ";
        cout << imjpResultsVector.back().logLength << endl; 
        cout << "The IMJP error probability is: " << imjpResultsVector.back().imjpErrorProbability << endl;
    }
}

void LowerBound::computeLastSingleChannel()
{
    int i; 
    int minabAlphabetSize;

    minabAlphabetSize = min(aChannelAlphabetSize, bChannelAlphabetSize); 

    lastSingleChannel = initialChannel; 
    
    lastSingleChannel.upgrade(lastSingleChannel, minabAlphabetSize); 

    for (i = 0; i < logIndexOfFirstJointChannel; i++)
    {
        if( aChannelTransformList.getTransform(i) == false )
        {
            lastSingleChannel = lastSingleChannel.minusTransform();
        }
        else // `+'-transform
        {
            lastSingleChannel = lastSingleChannel.plusTransform(); 
        }
        lastSingleChannel.upgrade(lastSingleChannel, minabAlphabetSize); 
    }
    
    lastSingleChannelIndex = aChannelTransformList.getChannelIndex(); 
    lastSingleChannelLogLength = aChannelTransformList.getLogLength(); 
}

void LowerBound::computeLowerBound(int whatToPrint)
{ 
    int i; 
    int aTransform; 
    int bTransform; 
    JointChannel jc; 
    Results res; 

    imjpErrorProbability.clear();

    assert(areTransformListsLoaded);

    computeLastSingleChannel();
    
    jc =  JointChannel(lastSingleChannel, aChannelAlphabetSize, bChannelAlphabetSize);
    imjpErrorProbability.push_back( jc.imjpErrorProb() ); 

    if(whatToPrint == 1)
    {
        cout << "********************************" << endl;
        cout << "i = " << logIndexOfFirstJointChannel + 1 << endl; 
        cout << "The IMJP error probability = " << jc.imjpErrorProb() << endl; 
        jc.printSelf();
    }

    for (i = logIndexOfFirstJointChannel+1 ; i < logLength ; i ++)
    {
        aTransform = aChannelTransformList.getTransform(i);
        bTransform = bChannelTransformList.getTransform(i);

        jc = JointChannel(jc, aTransform, bTransform, aChannelAlphabetSize, bChannelAlphabetSize); 
        imjpErrorProbability.push_back( jc.imjpErrorProb() );

        if(whatToPrint == 1)
        {
            cout << "********************************" << endl;
            cout << "i = " << i + 1 << endl; 
            cout << "The IMJP error probability = " << jc.imjpErrorProb() << endl; 
            jc.printSelf();
        }

        if(whatToPrint == 2)
        {
            cout << "i = " << i+1 << endl; 
        }
    }

    res.aChannelIndex = aChannelTransformList.getChannelIndex(); 
    res.bChannelIndex = bChannelTransformList.getChannelIndex(); 
    res.logLength = logLength; 
    res.imjpErrorProbability = imjpErrorProbability.back(); 

    imjpResultsVector.push_back(res); 
}

void LowerBound::printToFile(ofstream &filename) 
{
    assert(filename.is_open() == true); 
    filename << "************************************************************" << endl; 
    filename << "Initial channel" << endl;
    filename << "---------------" << endl;
    initialChannel.printToFile(filename); 
    filename << "************************************************************" << endl; 
    filename << "The a-channel Alphabet Size is: " << aChannelAlphabetSize << endl; 
    aChannelTransformList.printToFile(filename); 
    filename << "The b-channel Alphabet Size is: " << bChannelAlphabetSize << endl;
    bChannelTransformList.printToFile(filename); 
    filename << "The two transform lists differ on index " << logIndexOfFirstJointChannel << endl;
    filename << "************************************************************" << endl; 
    filename << "The IMJP results vector is: " << endl;

    for(myfloat imjp : imjpErrorProbability)
    {
        filename << setw(12) << imjp << ", "; 
    }
    filename << endl; 
    filename << "************************************************************" << endl; 
    filename << "The latest Joint Channel Checked is: (aIndex, bIndex) = (";
    filename << imjpResultsVector.back().aChannelIndex << ", ";
    filename << imjpResultsVector.back().bChannelIndex <<  ") , at log-length ";
    filename << imjpResultsVector.back().logLength << endl; 
    filename << "The IMJP error probability is: " << imjpResultsVector.back().imjpErrorProbability << endl;
    filename << "************************************************************" << endl; 
}


void LowerBound::printResultsToFile(ofstream &filename) 
{
    assert(filename.is_open() == true); 

    for(Results res : imjpResultsVector)
    {
        filename << "(a,b) = (" << setw(4) << res.aChannelIndex << ", " << setw(4) << res.bChannelIndex;
        filename << ") ; Pe_imjp = " << setw(12) << res.imjpErrorProbability << endl; 
    }
}

myfloat LowerBound::getIMJP() const
{
    return imjpErrorProbability.back();
}
