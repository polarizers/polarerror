/* MergedLRsDataStructure.h   
 *
 * Copyright (C) 2010 Ido Tal <idotal@ieee.org> and Alexander Vardy <avardy@ucsd.edu>
 *
 * This file is part of polarError. If you wish to use it, please cite: 
 * B. Shuval, I. Tal ``A Lower Bound on The Probability of Error of Polar Codes over BMS Channels,''
 * arxiv:1701.01628.
 * 
 * and 
 *
 * I. Tal, A. Vardy ``How To Construct Polar Codes,''  IEEE Transactions on Information Theory, vol.
 * 59, no. 10, pp. 6562-6582, October 2013. 
 *
 * polarBounds is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or (at
 * your option) any later version.
 *
 * polarError is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with polarError; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */


#ifndef MERGED_LRS_DATA_STRUCTURE
#define MERGED_LRS_DATA_STRUCTURE

#include <iostream>
#include "Channel.h"

class ChannelMergeParameters;

struct MergedLRDatum
{
    MergedLRDatum *prev, *next; /// An ordered linked list. The first element has highest LR, last element has lowest.
    int firstLRIndex, lastLRIndex; /// the indices firstLRIndex--lastLRIndex in mlrd whose elements were merged into this datum
    myfloat peven; /// probability of the first (even) output symbol
    myfloat podd; /// probability of the second (odd) output symbol
    myfloat mergeWithNextCapacityDelta; /// The amount by which the channel capacity will change if the current LR pair is merged with the next one
    int indexInHeap; /// we will also have a binary heap ordered according to mergeWithNextCapacityDelta
};

class MergedLRsDataStructure
{
public:
    MergedLRsDataStructure(const Channel &channel, const ChannelMergeParameters *ammparam);
    ~MergedLRsDataStructure();
    /// pop the head of the heap, and update next and prev links of those still in the heap
    MergedLRDatum *pop();

    /// The key (mergeWithNextCapacityDelta) has changed, and thus the heap must be reorganized.
    /// We require that before the key changed, the heap was valid: every change of key means calling this function immediately.
    void keyChanged(MergedLRDatum *mlrd);
    void writeCapacityDifference(MergedLRDatum *mlrd);
    int getCount() const;
    void createNewProbsArray(myfloat *&newProbsArray, int &newOutputAlphabetCount, const Channel *channel);
    Channel createNewChannel(const Channel &oldChannel);
    void printSelf(std::ostream& output) const;
    void sanityCheck() const;
    myfloat getTopKey() const;
    const MergedLRDatum *getHead() const; // return the first item in the linked list

    void mergeUntilGood(int verbosity = 0);

private:
    const ChannelMergeParameters *mmparam;
    int count;
    MergedLRDatum *array;
    MergedLRDatum **heap;
    //GetCapacityDifference *getCapacityDifference;

    void buildArray(const Channel &channel);
    void buildHeap();


    const myfloat& getHeapKey(int index);

    void heapSwitch(int son);
    bool bubbleDown(int father);
    bool bubbleUp(int son);

    void getSons(int father, int &leftSon, int &rightSon);
    void getFather(int son, int &father);
};

#endif // MERGED_LRS_DATA_STRUCTURE
