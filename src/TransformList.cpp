 /* TransformList.cpp
 *
 * Copyright (C) 2018 Boaz Shuval <bshuval@campus.technion.ac.il> and Ido Tal <idotal@ieee.org> 
 *
 * This file is part of polarError. If you wish to use it, please cite: 
 * B. Shuval, I. Tal ``A Lower Bound on The Probability of Error of Polar Codes over BMS Channels,''
 * arxiv:1701.01628.
 * 
 * and 
 *
 * I. Tal, A. Vardy ``How To Construct Polar Codes,''  IEEE Transactions on Information Theory, vol.
 * 59, no. 10, pp. 6562-6582, October 2013. 
 *
 * polarError is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or (at
 * your option) any later version.
 *
 * polarError is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with polarError; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */  
#include "TransformList.h"
#include "myfloat.h"
#include "assert.h"  
#include "math.h"
#include <iostream>
#include <algorithm>

using namespace std; 

TransformList::TransformList()
{
}

TransformList::TransformList(int alogLength, int achannelIndex)
{
    int curChan;

    assert(alogLength > 0);
    
    logLength = alogLength; 

    if( achannelIndex == 0)
    {
        channelIndex = achannelIndex; 
    }
    else
    {
        assert( floor(log2( (myfloat) achannelIndex) ) < logLength ); 
        channelIndex = achannelIndex; 
    }
    

    transformVector.reserve(logLength); 

    curChan = achannelIndex; 

    for( int i = 0; i < alogLength; i++)
    {
        if ( curChan > 0)
        {
            transformVector.push_back( (bool) (curChan % 2) );
            curChan /= 2; 
        }
        else
        {
            transformVector.push_back( 0 ); 
        }
    }

    reverse(transformVector.begin(), transformVector.end()); 

    curChan = 0;

    for( int i = 0; i < alogLength; i++)
    {
        curChan*=2;
        if(transformVector.at(i) == true)
        {
            curChan += 1;
        }                     
        channelIndexVector.push_back(curChan); 
    }

}

TransformList::~TransformList()
{
}

void TransformList::printSelf() const
{

    cout << "The Log-Length is: " << getLogLength() << endl;
    cout << "The channel Index is: " << getChannelIndex() << endl;

    cout << "The transform list is: "  ;
        for ( bool curTransform : transformVector)
        {
            if (curTransform == false)
            {
                cout << " -" ; 
            }
            else // curTransfomr == true
            {
                cout << " +"; 
            }
        }
    cout << endl;

    cout << "The channel list is: "  ;
        for ( int curChan : channelIndexVector )
        {
            cout << curChan << " "; 
        }
    cout << endl; 
}

int TransformList::getLogLength() const
{
    return logLength;
}

int TransformList::getChannelIndex() const
{
    return channelIndex;
}

bool TransformList::getTransform(int k) const
{
    assert( k < logLength ); 

    return transformVector.at(k);
}


int TransformList::getChannelIndex(int k) const
{
    assert( k < logLength ); 

    return channelIndexVector.at(k); 
}



void TransformList::printToFile(ofstream &filename) const
{
    assert(filename.is_open() == true); 

    filename << "The Log-Length is:     " << getLogLength() << endl;
    filename << "The channel Index is:  " << getChannelIndex() << endl;

    filename << "The transform list is: "  ;
        for ( bool curTransform : transformVector)
        {
            if (curTransform == false)
            {
                filename << " -" ; 
            }
            else // curTransfomr == true
            {
                filename << " +"; 
            }
        }
    filename << endl;

    filename << "The channel list is:   "  ;
        for ( int curChan : channelIndexVector )
        {
            filename << curChan << " "; 
        }
    filename << endl; 
}                                       
