/* ChannelMergeParameters.cpp   
 *
 * Copyright (C) 2010 Ido Tal <idotal@ieee.org> and Alexander Vardy <avardy@ucsd.edu>
 *
 * This file is part of polarError. If you wish to use it, please cite: 
 * B. Shuval, I. Tal ``A Lower Bound on The Probability of Error of Polar Codes over BMS Channels,''
 * arxiv:1701.01628.
 * 
 * and 
 *
 * I. Tal, A. Vardy ``How To Construct Polar Codes,''  IEEE Transactions on Information Theory, vol.
 * 59, no. 10, pp. 6562-6582, October 2013. 
 *
 * polarBounds is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or (at
 * your option) any later version.
 *
 * polarError is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with polarError; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */


#include "ChannelMergeParameters.h"
#include "Channel.h"
#include "MergedLRsDataStructure.h"
#include <assert.h>

const int maxDenum = 1000000;

// --- PairMerge ----------------------------------------------------------------------------------
myfloat PairMerge::getCapacityDifference(const MergedLRDatum *current) const
{
    const MergedLRDatum *prev;
    const MergedLRDatum *next;

    prev = current->prev;
    next = current->next;

    //if (next != nullptr)
    if (prev != nullptr && next != nullptr) // don't touch either boundary LRs
    {
        return getPairCapacityDifference(current->peven, current->podd, next->peven, next->podd);
    }
    else
    {
        return plusInfinity;
    }

}

void PairMerge::updateProbsAfterMerge(const MergedLRDatum *current) const
{
    MergedLRDatum *next;

    next = current->next;

    updatePairProbsAfterMerge(next->peven, next->podd, current->peven, current->podd);
}

void PairMerge::createNewProbsArray(myfloat *newProbsArray, const Channel *channel, const MergedLRsDataStructure &ds) const
{
    int currentNewLR;
    const MergedLRDatum *mlrd;

    mlrd = ds.getHead();

    currentNewLR = 0;

    while ( mlrd != nullptr )
    {
        createNewProbsArray_Pair(newProbsArray[2*currentNewLR], newProbsArray[2*currentNewLR+1], mlrd->firstLRIndex, mlrd->lastLRIndex, channel);
        mlrd = mlrd->next;
        currentNewLR++;
    }
}

void PairMerge::updateLRIndices(const MergedLRDatum *mlrd) const
{
    MergedLRDatum *nextDatum;

    nextDatum = mlrd->next;

    nextDatum->firstLRIndex = mlrd->firstLRIndex;
}

bool PairMerge::nextDatumKeyChanges() const
{
    return true;
}

bool PairMerge::prevDatumKeyChanges() const
{
    return true;
}

// --- ChannelMergeParameters -----------------------------------------------------------------------
ChannelMergeParameters::~ChannelMergeParameters()
{
}

bool ChannelMergeParameters::LRisInfinity(const myfloat &yeven, const myfloat &yodd)
{
    if (yodd <= 0.0 || (yeven/yodd == plusInfinity) )
    {
        return true;
    }
    else
    {
        return false;
    }
}

myfloat ChannelMergeParameters::mylogFrac(const myfloat &d1, const myfloat &d2)
{
    //cout << "d1 = " << d1 << ", d2 = " << d2 << " mylogFrac = " << mylog( (d1-d2)/(1+d2) ) << endl;

    const myfloat delta = (d1-d2)/(1+d2);

    if ( delta >= 1.0/maxDenum || delta <= -1.0/maxDenum )
    {
        return log(1.0 + d1)  - log (1.0 + d2);
    }
    else
    {
        return mylog( delta );
    }
}

myfloat ChannelMergeParameters::mylog(const myfloat &d)
{
    //cout << "d = " << d;

    if ( d >= 1.0/maxDenum || d <= -1.0/maxDenum)
    {
        //cout << " log = " << log(1.0+d) << endl;
        return log(1.0+d);
    }

    const myfloat d2 = d*d;
    const myfloat d3 = d2*d;
    const myfloat d4 = d2*d2;

    //cout << " log = " << d - d2/2 + d3/3 - d4/4 << endl;
    return d - d2/2 + d3/3 - d4/4;
}

void ChannelMergeParameters::getProbs(myfloat &yeven, myfloat &yodd, int LRIndex, const Channel *channel) const
{
    int i = LRIndex;

    yeven = channel->getProbOutputGivenInput(2*i, 0);
    yodd = channel->getProbOutputGivenInput(2*i+1, 0);
}

void ChannelMergeParameters::calcSumProbs(myfloat &sumyeven, myfloat &sumyodd, int firstLRIndex, int lastLRIndex, const Channel *channel) const
{
    int i;
    myfloat tempEven, tempOdd;

    ksEven.startNewSum();
    ksOdd.startNewSum();

    for ( i = firstLRIndex; i <= lastLRIndex; i++ )
    {
        getProbs(tempEven, tempOdd, i, channel);

        ksEven.insert(tempEven);
        ksOdd.insert(tempOdd);

    }

    sumyeven =  ksEven.calcSum();
    sumyodd =  ksOdd.calcSum();
}

// --- ChannelMergeParameters_stopWhenAlphabetSmallEnough -------------------------------------------

ChannelMergeParameters_stopWhenAlphabetSmallEnough::ChannelMergeParameters_stopWhenAlphabetSmallEnough(int aM)
{
    M = aM;
}

bool ChannelMergeParameters_stopWhenAlphabetSmallEnough::stoppingCondition(const MergedLRsDataStructure &ds) const
{
    if (2*ds.getCount() <= M)
    {
        return true;
    }
    else
    {
        return false;
    }

}

// --- ChannelMergeParameters_twoWayDegrade ---------------------------------------------------------
ChannelMergeParameters_twoWayDegrade::ChannelMergeParameters_twoWayDegrade(int aM) :
    ChannelMergeParameters_stopWhenAlphabetSmallEnough(aM)
{
}

myfloat ChannelMergeParameters_twoWayDegrade::getPairCapacityDifference(const myfloat &y1,const myfloat &y1bar, const myfloat &y2, const myfloat &y2bar) const
{
    const myfloat &alpha = y1;
    const myfloat &beta = y1bar;
    const myfloat &gamma = y2;
    const myfloat &delta = y2bar;

    myfloat d;

    if ( LRisInfinity(alpha,beta) )
    {
        if ( LRisInfinity(gamma,delta) )
        {
            return 0.0;
        }
        else
        {
            //std::cout << "alpha = " << alpha << " beta = " << beta << " gamma = " << gamma << " delta = " << delta;

            d = ( alpha*mylog(delta/(alpha+gamma)) +
                  gamma*mylogFrac(alpha/(gamma+delta), alpha/gamma) +
                  delta*mylog(alpha/(gamma+delta)) ) / log(2.0);

            //std::cout << " d = " << d << std::endl;

            assert( isnan(d) == false );

            return d;


        }
    }
    else
    {
        d = ( alpha*mylogFrac(beta/alpha,(beta+delta)/(alpha+gamma)) +
              beta*mylogFrac(alpha/beta, (alpha+gamma)/(beta+delta)) +
              gamma*mylogFrac(delta/gamma, (beta+delta)/(alpha+gamma)) +
              delta*mylogFrac(gamma/delta, (alpha+gamma)/(beta+delta)) ) / -log(2.0);

        assert( isnan(d) == false );

        return d;
    }

}

void ChannelMergeParameters_twoWayDegrade::updatePairProbsAfterMerge(myfloat &nextDatum_peven, myfloat &nextDatum_podd, const myfloat &currentDatum_peven, const myfloat &currentDatum_podd) const
{
    nextDatum_peven += currentDatum_peven;
    nextDatum_podd += currentDatum_podd;
}

void ChannelMergeParameters_twoWayDegrade::createNewProbsArray_Pair(myfloat &newyeven, myfloat &newyodd, int firstLRIndex, int lastLRIndex, const Channel *channel) const
{
    calcSumProbs(newyeven, newyodd, firstLRIndex, lastLRIndex, channel);
}

// --- ChannelMergeParameters_oneWayUpgrade ---------------------------------------------------------
ChannelMergeParameters_oneWayUpgrade::ChannelMergeParameters_oneWayUpgrade(myfloat aEpsilon)
{
    epsilon = aEpsilon;
}

myfloat ChannelMergeParameters_oneWayUpgrade::getPairCapacityDifference(const myfloat &y1,const myfloat &y1bar, const myfloat &y2, const myfloat &y2bar) const
{
    myfloat d;

    if (LRisInfinity(y1,y1bar) )
    {

        if (LRisInfinity(y2,y2bar) )
        {
            return 0.0;
        }
        else
        {
            return 1.0;
        }
    }
    else
    {
        //std::cout << "cap diff: " << (y1/y1bar)/(y2/y2bar) - 1.0 << std::endl;
        d = (y1/y1bar)/(y2/y2bar) - 1.0;

        assert( isnan(d) == false );

        return d;

    }
}

void ChannelMergeParameters_oneWayUpgrade::updatePairProbsAfterMerge(myfloat &nextDatum_peven, myfloat &nextDatum_podd, const myfloat &currentDatum_peven, const myfloat &currentDatum_podd) const
{
    myfloat lambda1;
    myfloat newEven, newOdd;

    // lambda1 is infinity
    if ( LRisInfinity(currentDatum_podd, currentDatum_peven) == true )
    {
        newEven = nextDatum_peven + nextDatum_podd + currentDatum_peven + currentDatum_podd;
        newOdd = 0.0;
    }
    else
    {
        lambda1 = currentDatum_peven/currentDatum_podd;

        newEven = currentDatum_peven + (nextDatum_peven+nextDatum_podd)/(1.0+1.0/lambda1);
        newOdd = currentDatum_podd + (nextDatum_peven+nextDatum_podd)/(1.0+lambda1);
    }
    nextDatum_peven = newEven;
    nextDatum_podd = newOdd;
}

bool ChannelMergeParameters_oneWayUpgrade::stoppingCondition(const MergedLRsDataStructure &ds) const
{

    if (ds.getTopKey() >= epsilon)
    {
        return true;
    }
    else
    {
        return false;
    }
}

void ChannelMergeParameters_oneWayUpgrade::createNewProbsArray_Pair(myfloat &newyeven, myfloat &newyodd, int firstLRIndex, int lastLRIndex, const Channel *channel) const
{
    myfloat sumyeven, sumyodd;
    myfloat firstyeven, firstyodd;

    calcSumProbs(sumyeven, sumyodd, firstLRIndex, lastLRIndex, channel);

    if ( firstLRIndex == lastLRIndex ) // only one element
    {
        newyeven = sumyeven;
        newyodd = sumyodd;
    }
    else
    {
        getProbs(firstyeven, firstyodd, firstLRIndex, channel);

        if ( LRisInfinity(firstyeven, firstyodd) == true )
        {
            newyeven = sumyeven + sumyodd;
            newyodd = 0.0;
        }
        else
        {
            myfloat lambda1;

            lambda1 = firstyeven/firstyodd;

            newyeven = (sumyeven + sumyodd) / (1.0 + 1.0 / lambda1);
            newyodd = (sumyeven + sumyodd) / (1.0 + lambda1);
        }
    }
}

// --- ChannelMergeParameters_twoWayUpgrade ------------------------------------------------------
ChannelMergeParameters_twoWayUpgrade::ChannelMergeParameters_twoWayUpgrade(int aM) :
    ChannelMergeParameters_stopWhenAlphabetSmallEnough(aM)
{
}

void ChannelMergeParameters_twoWayUpgrade::calcPrivates(const myfloat &ax, const myfloat &ay, const myfloat &prevEven, const myfloat &prevOdd, const myfloat &nextEven, const myfloat &nextOdd) const
{
    x = ax;
    y = ay;

    lambda1 = prevEven/prevOdd;
    lambda2 = x/y;
    lambda3 = nextEven/nextOdd;

    if ( LRisInfinity(prevEven, prevOdd) )
    {
        delta = y;
        gamma = delta*lambda3;

        alpha = x - gamma;
        beta = 0.0;

    }
    else
    {
        beta = (1.0/(lambda1 - lambda3))*(x - lambda3*y);
        delta = (1.0/(lambda1 - lambda3))*(lambda1*y-x);

        alpha = beta*lambda1;
        gamma = delta*lambda3;

    }
}

void ChannelMergeParameters_twoWayUpgrade::calcPrivates(const MergedLRDatum *mlrd) const
{
    assert(mlrd->next != nullptr && mlrd->prev != nullptr);

    calcPrivates(mlrd->peven, mlrd->podd, mlrd->prev->peven, mlrd->prev->podd, mlrd->next->peven, mlrd->next->podd);
}

myfloat ChannelMergeParameters_twoWayUpgrade::getCapacityDifference(const MergedLRDatum *mlrd) const
{
    myfloat d;

    if (mlrd->next == nullptr || mlrd->prev == nullptr)
    {
        return plusInfinity;
    }

    // current probability pair is (x,y)
    // mapped to (alpha,beta) and (gamma,delta)

    calcPrivates(mlrd);


    // We assume we don't have duplicate LRs at this point
    assert ( LRisInfinity(x, y) == false && LRisInfinity(mlrd->next->peven, mlrd->next->podd) == false);

    if ( LRisInfinity(mlrd->prev->peven, mlrd->prev->podd) )
    {
        d = -(gamma*mylog(1.0/lambda3) - x*mylog(1.0/lambda2) + delta*mylogFrac(lambda3, lambda2));

        assert( isnan(d) == false );
        return d;
    }
    else
    {
        // This assertion fails for: valgrind --db-attach=yes ./polarBounds --prob=0.11 -b 0111111111 --quantization-bins=10 -u
        // To the best of my understanding, it is a valgrind bug.
        assert(lambda1 != plusInfinity);

        d = -(alpha*mylog(1.0/lambda1)+beta*mylog(lambda1)+gamma*mylog(1.0/lambda3)+delta*mylog(lambda3)
              -x*mylog(1.0/lambda2)-y*mylog(lambda2));

        assert( isnan(d) == false );
        return d;
    }
}


void ChannelMergeParameters_twoWayUpgrade::updateProbsAfterMerge(const MergedLRDatum *mlrd) const
{

    calcPrivates(mlrd);

    assert( alpha >= 0.0 && beta >= 0.0 && gamma >= 0.0 && delta >= 0 );

    mlrd->prev->peven += alpha;
    mlrd->prev->podd += beta;

    mlrd->next->peven += gamma;
    mlrd->next->podd += delta;


    if ( LRisInfinity(mlrd->prev->peven, mlrd->prev->podd) == false && LRisInfinity(mlrd->next->peven, mlrd->next->podd) == true)
    {
        assert(false);
    }

    if ( LRisInfinity(mlrd->prev->peven, mlrd->prev->podd) == false && LRisInfinity(mlrd->next->peven, mlrd->next->podd) == false)
    {
        if ( ! ( (mlrd->prev->peven / mlrd->prev->podd) /  (mlrd->next->peven / mlrd->next->podd) >= 1.0001) )
        {
            std::cout << "fraction = " << (mlrd->prev->peven / mlrd->prev->podd) /  (mlrd->next->peven / mlrd->next->podd) << std::endl;
            assert(false);
        }
    }
}

void ChannelMergeParameters_twoWayUpgrade::createNewProbsArray(myfloat *newProbsArray, const Channel *channel, const MergedLRsDataStructure &ds) const
{
    int currentNewLR;
    int count = ds.getCount();
    const MergedLRDatum *mlrd;
    myfloat prevEven, prevOdd, nextEven, nextOdd;
    myfloat tempEven, tempOdd;
    myfloat prevEvenDelta, prevOddDelta, nextEvenDelta, nextOddDelta;

    int firstLRIndex, lastLRIndex, i;

    // initialize the array to 0
    for ( currentNewLR = 0; currentNewLR < count; currentNewLR++)
    {
        newProbsArray[2*currentNewLR] = newProbsArray[2*currentNewLR+1] = 0.0;
    }

    for (mlrd = ds.getHead(), currentNewLR = 0; mlrd != nullptr; mlrd = mlrd->next, currentNewLR++)
    {
        ksEven.startNewSum();
        ksOdd.startNewSum();
        ksNextEven.startNewSum();
        ksNextOdd.startNewSum();

        firstLRIndex = mlrd->firstLRIndex;
        lastLRIndex =  mlrd->lastLRIndex;

        if ( lastLRIndex+1 == channel->getOutputAlphabetSize()/2 ) //!!
        {
            assert ( firstLRIndex == lastLRIndex );
            assert( mlrd->next == nullptr );

            getProbs(tempEven, tempOdd, firstLRIndex, channel);
            newProbsArray[2*currentNewLR] += tempEven;
            newProbsArray[2*currentNewLR+1] += tempOdd;
            continue;
        }

        i = firstLRIndex;

        getProbs(prevEven, prevOdd, i, channel);

        ksEven.insert(prevEven);
        ksOdd.insert(prevOdd);

        i = lastLRIndex+1;
        getProbs(nextEven, nextOdd, i, channel);

        for ( i = firstLRIndex+1; i <= lastLRIndex; i++ )
        {
            getProbs(tempEven, tempOdd, i, channel);

            calcPrivates(tempEven, tempOdd, prevEven, prevOdd, nextEven, nextOdd);

            ksEven.insert(alpha);
            ksOdd.insert(beta);

            ksNextEven.insert(gamma);
            ksNextOdd.insert(delta);

        }

        prevEvenDelta =  ksEven.calcSum();
        prevOddDelta =  ksOdd.calcSum();
        nextEvenDelta =  ksNextEven.calcSum();
        nextOddDelta =  ksNextOdd.calcSum();

        newProbsArray[2*currentNewLR] += prevEvenDelta;
        newProbsArray[2*currentNewLR+1] += prevOddDelta;

        newProbsArray[2*(currentNewLR+1)] += nextEvenDelta;
        newProbsArray[2*(currentNewLR+1)+1] += nextOddDelta;
    }
}

void ChannelMergeParameters_twoWayUpgrade::updateLRIndices(const MergedLRDatum *mlrd) const
{
    MergedLRDatum *prevDatum;

    prevDatum = mlrd->prev;

    prevDatum->lastLRIndex = mlrd->lastLRIndex;
}

bool ChannelMergeParameters_twoWayUpgrade::nextDatumKeyChanges() const
{
    return true;
}
bool ChannelMergeParameters_twoWayUpgrade::prevDatumKeyChanges() const
{
    return true;
}
