 /* main.cpp
 *
 * Copyright (C) 2018 Boaz Shuval <bshuval@campus.technion.ac.il> and Ido Tal <idotal@ieee.org> 
 *
 * This file is part of polarError. If you wish to use it, please cite: 
 * B. Shuval, I. Tal ``A Lower Bound on The Probability of Error of Polar Codes over BMS Channels,''
 * arxiv:1701.01628.
 * 
 * and 
 *
 * I. Tal, A. Vardy ``How To Construct Polar Codes,''  IEEE Transactions on Information Theory, vol.
 * 59, no. 10, pp. 6562-6582, October 2013. 
 *
 * polarError is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or (at
 * your option) any later version.
 *
 * polarError is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with polarError; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */    
#include <iostream> 
#include <iomanip> 
#include <fstream>
#include <sstream>
#include <string>
#include <algorithm>
#include <unistd.h>
#include "assert.h"
#include "math.h"

#include "Channel.h"
#include "ChannelMergeParameters.h"
#include "MergedLRsDataStructure.h"
#include "JointChannel.h"
#include "TransformList.h"
#include "LowerBound.h"

using namespace std;

int main( int argc, char** argv )
 {
    Channel channel;
    ofstream output; 
    double pBSC; 
    int Ma, Mb;
    int aChannelIndex, bChannelIndex, logLength, channelSetw;
    int verbosity = 0;    
    
    string aChanString, bChanString;  

    // input parsing
    static const char *optString = "M:L:a:b:p:vh"; 
    
    int opt = getopt(argc, argv, optString);
    while( opt != -1)
    {
        switch(opt)
        {
            case 'M':
                Ma = atoi(optarg);
                Mb = atoi(optarg); 
                break;
                
            case 'L':
                logLength = atoi(optarg); 
                break; 

            case 'a':
                aChannelIndex = atoi(optarg);
                aChanString   = optarg; 
                break;

            case 'b': 
                bChannelIndex = atoi(optarg); 
                bChanString   = optarg; 
                break; 

            case 'p':
                pBSC = atof(optarg); 
                break;

            case 'v':
                verbosity = 1; 
                // verbosity = 2; 
                break; 

            case 'h':
                cout << " Usage: ./polarError -M <alphabet size> -L <log length> -a <a channel index> -b <b channel index> -p <BSC crossover probability> " << endl; 
                cout << " Add -v flag for progress indication " << endl; 
                cout << " -h prints this help message. " << endl; 
                return -1;
                break;

            default:
                break;
        }
        opt = getopt(argc,argv,optString); 
    }

    if (aChannelIndex > bChannelIndex)
    {
        swap(aChannelIndex, bChannelIndex); 
        swap(aChanString, bChanString); 
    }

    string outputFileName = "output_a_" + aChanString + "_b_" + bChanString + "_p_" + to_string((int)floor(100*pBSC)) + ".txt"; 

    channelSetw = ceil(logLength*log(2.0)/log(10.0)); 
    
    output.open(outputFileName); 
    channel.addProbOfConjugatePairGivenInputZero(pBSC, 1.0 - pBSC);
    channel.canonicalizeSelf();
    LowerBound lowerbound(channel, Ma, Mb); 
    lowerbound.generateTransformList(aChannelIndex, bChannelIndex, logLength); 
    lowerbound.computeLowerBound(verbosity); 

    
    output << "pBSC = " << setw(3) << pBSC  << " ; " ; 
    output << "logLength = " << setw(2) << logLength << " ; ";
    output << "(a,b) = (" << setw(channelSetw) << aChannelIndex << ",";
    output << setw(channelSetw) << bChannelIndex << ") ; "; 
    output << "peimjp = ";
    output << setw(12) << setprecision(10) << lowerbound.getIMJP() << endl; 
    output << "============================================================" << endl; 

    lowerbound.printToFile(output);
    output.close();

    return 0; 
}
