 /* Channel.h
 *
 * Copyright (C) 2018 Boaz Shuval <bshuval@campus.technion.ac.il> and Ido Tal <idotal@ieee.org> 
 *
 * This file is part of polarError. If you wish to use it, please cite: 
 * B. Shuval, I. Tal ``A Lower Bound on The Probability of Error of Polar Codes over BMS Channels,''
 * arxiv:1701.01628.
 * 
 * and 
 *
 * I. Tal, A. Vardy ``How To Construct Polar Codes,''  IEEE Transactions on Information Theory, vol.
 * 59, no. 10, pp. 6562-6582, October 2013. 
 *
 * polarError is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or (at
 * your option) any later version.
 *
 * polarError is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with polarError; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */                                                                          
#ifndef CHANNEL_H
#define CHANNEL_H

#include "myfloat.h" 
#include <vector>
#include <fstream>
#include <utility>


class Channel
{
    public:
        Channel();
        Channel(int projectedOutputAlphabetSize );
        Channel( const Channel &channel );
        ~Channel();

        Channel &operator=(const Channel &channel );

        // Even and odd output indices are conjugate pairs. 
        // Erasures are split to "positive" and "negative" erasures. 
        myfloat getProbOutputGivenInput(int outputIndex, int input) const; 
        myfloat getAbsDValue( int outputIndexEven ) const;
        myfloat getPairProb( int outputIndexEven ) const;

        int getOutputAlphabetSize() const;

        void addProbOfConjugatePairGivenInputZero(myfloat evenProb, myfloat oddProb);

        // return a vector of the absolute values of the D-values, sorted in descending order
        std::vector<myfloat> getAbsDValues(bool assumeCanonicalized = true) const;
        std::vector<myfloat> getLRgtreq1(bool assumeCanonicalized = true) const;

        // The following function is (currently) only implemented under the assumption of
        // a canonicalized channel. 
        std::vector<std::pair<myfloat,myfloat>> getLRpairvec(bool assumeCanonicalized = true) const;

        // Make all probabilities sum to 1
        void normalizeSelf();

        // Make all probabilities sum to 1, make even probabilities greater
        // than odd ones, and sort according to descending D-values
        void canonicalizeSelf();
        void printSelf();
        void printToFile(std::ofstream &filename);
        void printProbVector();
        void printStats();
        void printDValues();
        void printLRs();

        Channel minusTransform();
        Channel plusTransform();

        // upgrade to a channel with at most M output letters (M even), and
        // write to newChannel
        void upgrade(Channel &newChannel, int M);

        myfloat calcErrorProbability() const;
        myfloat calcBhattacharyya() const;
        myfloat calcTV() const;
        myfloat calcSymmetricEntropy() const;

        
    private:
        std::vector<myfloat> probGivenZero; 
        myfloat xlogy(const myfloat &x, const myfloat &y) const;
};

#endif // CHANNEL_H

