# A Lower Bound on the Probability of Error of Polar Codes Over BMS Channels

- [Introduction](#introduction) 
    - [References](#References)
- [Usage](#Usage)
    - [Single Channel Quantization](#Single-Channel-Quantization)
- [Installation](#Installation)
    - [Mac OS X](#mac-os-x)



## Introduction
This code implements Algorithm A from the paper [\[1\]](https://arxiv.org/abs/1701.01628) by Boaz Shuval and Ido Tal. 
The code incorporates code developed by Ido Tal and Alexander Vardy for their paper [\[2\]](https://doi.org/10.1109/TIT.2013.2272694), used with
permission. 

If you wish to use this code, please cite both papers. 

### References 
\[1\] [B. Shuval, I .Tal, ``A Lower Bound on the Probability of Error of Polar Codes over BMS
Channels,'' arXiv:1701.01628](https://arxiv.org/abs/1701.01628)

\[2\] [I. Tal, A. Vardy, ``How To Construct Polar Codes,'' IEEE Transactions on Information Theory,
vol. 59, no. 10, pp. 6562-6582, October 2013](https://doi.org/10.1109/TIT.2013.2272694)  


## Usage
The code supports the lower bound algorithm for any initial (discrete-output) BMS channel. The
command-line interface, however, is designed specifically for an initial channel that is a binary
symmetric channel (BSC). Run the code via the following command:

```
./polarError -M <alphabet size> -L <log length> -a <a channel index> -b <b channel index> -p <BSC crossover probability>
```
The parameters are as follows:

- `<alphabet size>` is the quantization level. I.e., it denotes the number of output symbols for the
  marginal channel. We assume the same alphabet size for both marginals. This must be even. 
- `<log length>` is the number of polar transforms the channel had undergone. Thus, the polar code
  has length `2^<log length>`. 
- `<a channel index>` This is the index number of the a-channel, counting from 0.  
- `<b channel index>` This is the index number of the b-channel, counting from 0.  
- `<BSC crossover probability>` is the crossover probability of the initial BSC. 

The application outputs its results onto a text file whose name is derived from the the run parameters. 

The source code itself allows for additional options, such as arbitrary initial BMS channels and different
quantization levels for the marginal channels. 

### Single Channel Quantization
Future versions of this code will enhance the command line interface and enable upgrading/degrading
a single channel (as in [\[2\]](https://doi.org/10.1109/TIT.2013.2272694)). In the mean time, upgrading can be done using this code by using
consecutive channel indices. 

## Installation 
This code has been compiled and tested both on OSX (Darwin 17.3.0) and Ubuntu (releases 14.04.05 LTS;
16.04.1 LTS). It was compiled using GCC. The code utilizes the Armadillo library, which can be
downloaded from [here](http://arma.sourceforge.net).  

### Mac OS X
In Mac OS X, the `gcc` command runs clang. The code assumes a GNU compiler. One can install `gcc`
using [homebrew](https://brew.sh). Before compiling, set the compiler environment variables. In the
terminal, this can be done with the following commands (assuming that gcc 7 was installed to the
default location using homebrew): 

```
CC=gcc-7
CXX=g++-7
export CC CXX
```

